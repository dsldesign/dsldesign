// (c) dsldesign, wasowski, tberger
// An in-place transformation implemented directly in Scala (no special
// frameworks).  Translates a featuremodel from the first to the second ecore
// meta-model (in dsldesign.featuremodels/model)
package dsldesign.featuremodels1.scala.transforms

import scala.jdk.CollectionConverters.*

import dsldesign.featuremodels1, featuremodels1.{Feature1, Group1, Model1, OrGroup1}
import dsldesign.featuremodels2, featuremodels2.{Feature2, Group2, Model2}
import dsldesign.scala.emf.*

object FeatureModel1ToFeatureModel2 extends CopyingTrafo[Model1, Model2]:

  val fm2Factory = featuremodels2.Featuremodels2Factory.eINSTANCE

  extension (self:Feature1)
    def isSolitary(subfeature: Feature1): Boolean =
      !self.getGroups.asScala
        .exists {_.getMembers.asScala.exists(_ == subfeature)}

  def convertGroup (self: Group1): Group2 =
    { if (self.isInstanceOf[OrGroup1])
        fm2Factory.createOrGroup2
      else
        fm2Factory.createXorGroup2
    } before {
      _.getMembers.addAll(self.getMembers.asScala.map(convertFeature).asJava)
    }

  def convertFeature (self: Feature1): Feature2 =
    fm2Factory.createFeature2 before { f =>
      f.setName (self.getName)
      f.getSolitarySubfeatures.addAll (
        self.getSubfeatures.asScala
        .filter (self.isSolitary)
        .map (convertFeature).asJava)
      f.getGroups.addAll (self.getGroups.asScala.map (convertGroup).asJava)
  }

  def convertModel (self: Model1): Model2 =
    fm2Factory.createModel2 before { m =>
      m.setName(self.getName)
      m.setRoot(convertFeature(self.getRoot))
    }

  override def run (self: Model1): Model2 =
    convertModel (self)


// A main function to help running an example of the above trafo

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

@main def featureModel1ToFeatureModel2Main =
  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", XMIResourceFactoryImpl ())

  // register the package magic (impure)
  featuremodels1.FeatureModels1Package.eINSTANCE.eClass
  featuremodels2.Featuremodels2Package.eINSTANCE.eClass

  // Load the fsm model
  val featuremodel1 = 
    loadFromXMI ("dsldesign.featuremodels/test-files/test-00.xmi")

  // Run the transformation
  val featuremodel2 = FeatureModel1ToFeatureModel2.run (featuremodel1)

  // Save the new model
  val path = "dsldesign.featuremodels/test-files/test-00-transformed-by-Scala.xmi"
  saveAsXMI (path) (featuremodel2)
