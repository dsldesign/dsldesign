// (c) dsl.design, wasowski, tberger
package dsldesign.featuremodels1.scala.constraints

import scala.jdk.CollectionConverters.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import dsldesign.featuremodels1.*
import dsldesign.scala.emf.*

// Write your constraints here

val invariants: List[Constraint] = List {

  inv[Model1] { m => true }

}

// A main function to run the above constraints with 
// ./gradlew :dsldesign.featuremodels.scala:run

@main def checkConstraints =
  // Register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // Register the package magic (impure)
  FeatureModels1Package.eINSTANCE.eClass

  // Load the XMI file, change file name here --v
  val uri: URI = URI.createURI ("../dsldesign.featuremodels/test-files/test-00.xmi")
  val resource: Resource = ResourceSetImpl ().getResource (uri, true)

  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content: Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).asScala

  if content.forall { eo => invariants.forall (_ check eo) } 
    then println ("All constraints are satisfied!")
    else println ("Some constraint is violated!")
