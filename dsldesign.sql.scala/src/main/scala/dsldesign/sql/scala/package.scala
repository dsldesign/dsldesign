// (c) dsldesign, wasowski, tberger
// This is the main driver for sql constraints
package dsldesign.sql.scala

import scala.jdk.CollectionConverters.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import dsldesign.scala.emf.*
import dsldesign.sql.{Model, SqlPackage}

@main def main =

  // Register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // Register the the meta-model packages (quite a few in this exercise, impure)
  SqlPackage.eINSTANCE.eClass

  // load the XMI file (create one!), change path and file name here
  val uri: URI = URI.createURI ("../dsldesign.sql/test-files/test-00.xmi")
  val resource: Resource = ResourceSetImpl ().getResource (uri, true)

  val content: Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).asScala

  if content.forall { eo => constraints.invariants.forall (_._2 check eo) } 
    then println ("All constraints are satisfied!")
    else println ("Some constraint is violated!")
