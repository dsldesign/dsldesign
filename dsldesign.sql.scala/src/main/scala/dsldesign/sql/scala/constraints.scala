// (c) dsldesign, wasowski, tberger
package dsldesign.sql.scala.constraints

import scala.jdk.CollectionConverters.* // for natural access to EList
import dsldesign.scala.emf.*
import dsldesign.sql.*

val invariants = List[(String, Constraint)] (

  // Change true for a constraint
  "Empty constraint" -> inv[Model] { self => true },

  // Add more constraints here

)
