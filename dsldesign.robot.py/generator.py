# (c) dsldesign, wasowski, berger
# dynamic code is generated using generator.py from dsldesign libraries

import collections
import itertools
import re
import sys
from jinja2 import FileSystemLoader, Environment
from pyecore.resources import ResourceSet, URI
from pyecore.utils import DynamicEPackage

class GeneratorCtx:
    
    def __init__(self, fname):
        ffname = "../dsldesign.robot/model/robot.ecore"
        print(f'Loading the meta-model {ffname}', file=sys.stderr)
        self.resource_set = ResourceSet()
        self.resource = self.resource_set.get_resource(URI(ffname))
        self.robot_mmodel = self.resource.contents[0]
        self.Robot = DynamicEPackage(self.robot_mmodel)
        self.resource_set.metamodel_registry[self.robot_mmodel.nsURI] = \
                self.robot_mmodel
        self.fname = fname
        print(f'Loading the model {self.fname}', file=sys.stderr)
        self.model = self.resource_set.get_resource(URI (fname)).contents[0]
        self.__modes()
        self.__base_ids()
        self.__derive_ids(self.model)
    
    def primes(self):
        not_prime = collections.defaultdict (list)
        q = 1
        while True:
            if q in not_prime:
                for p in not_prime[q]: not_prime[p + q].append(p)
                del not_prime[q]
            else:
                yield q
                not_prime[q * q] = [q]
            q += 1
    
    def SNAKE(self, name):
        name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', name)
        return "_" + name.upper()

    def __modes(self):
        self.modes = []
        old_modes = [self.model]
        while old_modes:
            children = [ parent.modes for parent in old_modes ]
            new_modes = [ m for modes in children for m in modes ]
            self.modes.extend (old_modes)
            old_modes = new_modes

    def __base_ids(self):
        prime = self.primes()
        for m in self.modes:
            m.state_id = next(prime)
            m.SNAKE_NAME = self.SNAKE(m.name)

    def __derive_ids(self, parent):
        for m in parent.modes:
            m.state_id = f"{parent.state_id}*{m.state_id}"
            self.__derive_ids(m)

    def isMove(self, a): return isinstance(a, self.Robot.AcMove)
    def isTurn(self, a): return isinstance(a, self.Robot.AcTurn)
    def isDock(self, a): return isinstance(a, self.Robot.AcDock)

    def generate_expr(self, expr):
        """Translate a Robot expression to Python."""
        if isinstance(expr, self.Robot.RndI):
            return "(random.randrange(0, 2000) / 1000.0)"
        elif isinstance(expr, self.Robot.CstI):
            return str(expr.value)
        elif isinstance(expr, self.Robot.Minus):
            return - self.evaluate_expr (expr.aexpr)
        elif isinstance(expr, self.Robot.BinExpr):
            left = str(self.evaluate_expr (expr.left))
            right = str(self.evaluate_expr (expr.right))
            if expr.ope == '+': return '(' + left + right + ')'
            elif expr.ope == '-': return '(' + left - right + ')'
            elif expr.ope == '*': return '(' + left * right + ')'
            elif expr.ope == '\\': return '(' + left / right + ')'
            else: assert False, expr.ope

    def generate_trigger(self, ev):
        if ev == self.Robot.Event.EV_CLAP: return "ev_clap"
        if ev == self.Robot.Event.EV_OBSTACLE: return "ev_obstacle"
        assert False, ev

def main(args = None):
    ctx = GeneratorCtx("../dsldesign.robot/test-files/random-walk.robot.xmi")
    env = Environment(
            loader = FileSystemLoader("."),
            trim_blocks = True,
            lstrip_blocks = True,
    )
    template = env.get_template("controller.py.jinja")
    print(template.render(ctx = ctx))

if __name__ == '__main__':
    main()
