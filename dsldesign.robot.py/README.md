# A code generator in python for the .robot language.

##  Installing the prerequisites

The requirements.txt file lists the necessary dependencies. If you use
virtualenv:

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Your system may require `pip3` instead of `pip` (if the latter links
to `pip2`). This will install the required libraries, including the
template generation engine jina2 that we are using.

## Running the code generator

Just invoke `python generator.py` in this directory (or `python3` if
this is what it's called on your system).

A path to the source file (random-walk) is presently hard-coded in the
generator (this is easy to change, to take it form the command line),
and the generated code is output to the standard output. You can
simply redirect it, to store the output in a file:

```
python generator.py > generated_controller.py
```

## Running the generated code

If you want to play with the generated code in the simulator, copy the
obtained file to `dsldesign.robot.turtlebot3/dsldesign_robot_turtlebot3`
and follow the instructions in `dsldesign.robot.turtlebot3/README.md`
just replacing `controller.py` with the newly generated file.
