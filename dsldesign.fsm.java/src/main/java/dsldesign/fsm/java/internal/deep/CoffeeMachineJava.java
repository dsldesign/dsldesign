/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/** An example model, CoffeeMachine, in the fsm language, the variant using
  * deep embedding in Scala (an internal dSL). We are accessing the DSL from
  * Java to show that internal DSLs in Java also work, at some syntactic cost.
  */
package dsldesign.fsm.java.internal.deep;

import dsldesign.fsm.*;
import dsldesign.fsm.scala.internal.deep.*;

class CoffeeMachineJava 
  implements dsldesign.fsm.java.internal.deep.DeepFsmJava {

  /** Note the lack of semicolons, as the entire model is a single Java
    * expression.
    */
  dsldesign.fsm.Model m = 

    state.machine ("coffeeMachine")

      .initial ("initial")
        .input ("coin")   .output ("what drink do you want?") .target ("selection")
        .input ("idle")                                       .target ("initial")
        .input ("break")  .output ("machine is broken")       .target ("deadlock")

      .state ("selection")
        .input ("tea")    .output ("serving tea")             .target ("making tea")
        .input ("coffee") .output ("serving coffee")          .target ("making coffee")
        .input ("break")  .output ("machine is broken!")      .target ("deadlock")

     .state ("making coffee")
       .input ("done")    .output ("coffee served. Enjoy!")   .target ("initial")
       .input ("break")   .output ("machine is broken!")      .target ("deadlock")

     .state ("making tea")
       .input ("done")    .output ("tea served. Enjoy!")      .target ("initial")
       .input ("break")   .output ("machine is broken!")      .target ("deadlock")

     .state ("deadlock")

  .end ();

}

