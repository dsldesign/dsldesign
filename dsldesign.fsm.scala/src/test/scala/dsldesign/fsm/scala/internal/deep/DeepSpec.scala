// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.internal.deep

import scala.annotation.compileTimeOnly
import scala.jdk.CollectionConverters.*
import scala.language.postfixOps

import dsldesign.fsm
import dsldesign.fsm.scala.eval

// Example scenario tests for deeply embedded DSL for Finite State Machines
class DeepSpec extends
  org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.should.Matchers,
  org.scalatest.Inside:

  "transition without output" in {
    val m: fsm.Model =
      (state machine "m1"
        initial "s" input "i" target "s"
      end)

    val s = m.getMachines.get (0).getInitial
    eval (s) ("i") should be (Some (s -> None))
  }

  "transition without input" in {
    """
    val m =
      (state machine "m1"
        initial "s1" output "coin" target "selection"
      end)
    """ shouldNot compile
  }

  "machine without initial" in {
    """
      val m =
        (state machine "m1"
          state "s1" output "coin" target "selection"
        end)
    """ shouldNot compile
  }

  "machine with initial later in the definition" in {

    // Test that an expression compiles (syntax and types)
    val M: fsm.Model =
      (state machine "m"
        state "s1" input "i" target "s2"
        initial "s2" input "i" target "s1"
      end)

    // Access (bind) some syntax elements and check basic properties on them
    val m: fsm.FiniteStateMachine = M.getMachines.get (0)
    val Some (s1) = m.getStates.asScala.find { _.getName=="s1" }
    val s2: fsm.State = m.getInitial

    s2.getName should be ("s2")
    s1.getName should be ("s1")

    // Check semantics by using the testable interface of the interpreter
    eval (s2) ("i") should be { Some (s1 -> None) }
    eval (s1) ("i") should be { Some (s2 -> None) }
  }

  // And so on; see shallow variant for an example of property-based tests
