// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.internal.shallow

// Example scenario tests for shallowly embedded DSL for Finite State Machines
class ShallowSpec
  extends org.scalatest.freespec.AnyFreeSpec,
          org.scalatest.matchers.should.Matchers,
          org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  val deadlock = state

  "t0 composed t1 on random input not handled by t1 is the same as t" in {

    forAll { (inputStr: String) =>
      forAll { (inputStr1: String) =>
        whenever (inputStr != inputStr1) {
          lazy val t0: State = state input (inputStr) output (inputStr) target (t0)
          val t1 = t0 input (inputStr1) output (inputStr1) target (deadlock)

          t0.step (inputStr) should be { t1.step (inputStr) }

          val t2 = state input (inputStr1) output (inputStr1) target (deadlock)
          t2.step (inputStr1) should be { t1.step (inputStr1) }
        }
      }
    }
  }
