// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala

import scala.language.implicitConversions
import scala.util.{Success, Failure}

import org.parboiled2.*

import dsldesign.fsm.scala.adt.*

class FsmParserSpec extends
  org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.should.Matchers,
  org.scalatest.Inside:

  given Conversion[String, FsmParser] with
    def apply (input: String): FsmParser = FsmParser (input)

  "Nonterminals" - {

    val machine =
    """machine Punctuation [
         state Possesive [
           on Subject output Verb and go to Sentence
         ]
         initial Tense
       ]
    """

    val broken =
    """machine Punctuation [
         state Possesive [
           on S output V and go to Ton S output V and go to T
         ]
         initial Tense
       ]
    """

    val trs =
      Map ("Possesive" -> List (Transition ("Sentence", "Subject", "Verb")))
    val ast =
      FiniteStateMachine ("Punctuation", List ("Possesive"), trs, "Tense")


    "model" in {
      s"  ${machine} ${machine}${machine} ".model.run () shouldBe
        Success (Model ("",List(ast,ast,ast)))
    }

    "machineBlock" - {
      "positive 1" in { machine.machineBlock.run () shouldBe Success (ast) }

      "positive 2: empty state machine" in {
        " machine M [ state S [ ] initial S ] ".machineBlock.run () shouldBe
          Success (FiniteStateMachine ("M", List ("S"), Map ("S" -> Nil), "S"))
      }

      "positive 3: whitespace after the last state" in {
        " machine M [ initial S state S [ ] ] ".machineBlock.run () shouldBe
          Success (FiniteStateMachine ("M", List ("S"), Map ("S" -> Nil), "S"))
      }

      "negative" in { broken.machineBlock.run ().toOption shouldBe empty }
    }

    "stateBlock" - {
      "positive 1: full blown transition" in {
        "state P [ on input I output O and go to T ]".stateBlock.run () shouldBe
          Success ("P" -> List (Transition ("T","I","O")))
      }

      "positive 2: minimal whitespace" in {
        "state P[on I go to T]".stateBlock.run() shouldBe
          Success ("P" -> List (Transition ("T","I")))
      }

      "positive 3: empty state" in {
        "state P".stateBlock.run() shouldBe Success ("P" -> Nil)
      }

      "positive 4: empty state with brackets" in {
        "state P[]".stateBlock.run() shouldBe Success ("P" -> Nil)
      }

      "positive 5: empty state with brackets and space" in {
        "state P[ ]".stateBlock.run() shouldBe Success ("P" -> Nil)
      }

      "positive 6: two transitions whitespace" in {
        "state P[on I go to T on J go to U]".stateBlock.run() shouldBe
          Success ("P" -> List (Transition ("T","I"), Transition ("U","J")))
      }

      "negative 1: transitions without a separator" in {
        "state P[on S go to Ton S go to T]".stateBlock.run() shouldBe
          Success ("P" -> Nil)
          // Why? The rule will fail on the optional state block,
          // and the parser just stops on the accepted prefix 'state P'.
      }
    }


    "transition" - {
      "positive 1: full" in {
        "on input I output O and go to S".transition.run() shouldBe
          Success (Transition("S","I","O"))
      }

      "positive 2: no output" in {
        "on I go to T".transition.run() shouldBe Success (Transition ("T","I"))
      }

      "positive 3: input, no output" in {
        "on input I go to T".transition.run() shouldBe
          Success (Transition("T","I"))
      }

      "negative" in { "onI goto T".transition.run().toOption shouldBe empty }
    }


    "outputClause" in {
      "output Verb and ".outputClause.run () shouldBe Success ("Verb")
    }

    "targetClause" - {
      "positive" in {
        "go to Article".targetClause.run () shouldBe Success ("Article")
      }

      "negative" in {
        "gotoArticle".targetClause.run ().toOption shouldBe empty
      }
    }
  }

  "Terminals" - {

    "EString" - {
      "quoted" in {
        "\"English fun!\"".EString.run () shouldBe Success ("English fun!")
      }

      "not quoted" in {
        "English".EString.run () shouldBe Success ("English")
        "Verb".EString.run () shouldBe Success ("Verb")
      }
    }

    "Whitespace" - {
      "WS does not crash on actual whitespace" in {
        " \f\t\r\n".WS.run ().toOption shouldBe defined
      }

      "WS does not crash on non-whitespace" in {
        "ftrn".WS.run ().toOption shouldBe empty
      }
    }

    "ID" - {
      "ID returns the ID" in {
        "Conjugation".ID.run () shouldBe Success ("Conjugation")
        "Verb_42a".ID.run () shouldBe Success ("Verb_42a")
      }

      "ID consumes subsequent whitespace" in {
        "Conjugation\r \n\f\t".ID.run () shouldBe Success ("Conjugation")
      }

      "ID fails on an integer" in {
        "42".ID.run () should matchPattern { case
          Failure (ParseError (Position (0,1,1), Position (0,1,1), _)) => }
      }

      "ID fails on a string literal" in {
        "\"Declination\"".ID.run () should matchPattern { case
          Failure (ParseError (Position (0,1,1), Position (0,1,1), _)) => }
      }
    }

    "STRING" - {
      "Empty" in {
        "\"\"".STRING.run () shouldBe Success ("")
      }

      "Returns the simple string" in {
        "\"Verb\"".STRING.run () shouldBe Success ("Verb")
      }

      "Consumes subsequent whitespace" in {
        "\"Conjugation\"\r \n\f\t".STRING.run () shouldBe
          Success ("Conjugation")
      }

      "Fails on an integer" in {
        "42".STRING.run () should matchPattern { case
          Failure (ParseError (Position (0,1,1), Position (0,1,1), _)) => }
      }

      "Fails on an ID" in {
        "Declination".STRING.run () should matchPattern { case
          Failure (ParseError (Position (0,1,1), Position (0,1,1), _)) => }
      }
    }
  }

  "Large tests" - {

    def parse (fname: String) =
      scala.io.Source.fromFile (fname)
        .mkString
        .model
        .run ()

    val p = "../dsldesign.fsm/test-files/"

    "Complete.fsm" in {
      parse (s"${p}Complete.fsm").toOption shouldBe defined
    }

    "CompleteMinWhiteSpace.fsm" in {
      parse (p+"CompleteMinWhiteSpace.fsm").toOption shouldBe defined
    }

    "CompleteMaxWhiteSpace.fsm" in {
      parse (p+"CompleteMaxWhiteSpace.fsm").toOption shouldBe defined
    }
  }
