// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.transforms

import java.io.PrintWriter

import dsldesign.scala.emf
import dsldesign.fsm
import dsldesign.fsm.scala.transforms.{fsmToJava, fsmToDot}

class FsmCodeGeneratorSpec extends 
  org.scalatest.freespec.AnyFreeSpec, 
  org.scalatest.matchers.should.Matchers:

  fsm.FsmPackage.eINSTANCE.eClass
  val m: fsm.Model = 
    emf.loadFromXMI ("../dsldesign.fsm/test-files/CoffeeMachine.xmi")

  "FsmToJava" - {

    "just run the trafo for manual inspection"  in {
      val java = fsmToJava.compileToJava (m.getMachines.get (0))

      new PrintWriter ("test-out/CoffeeMachine.java"):
        write (java)
        close

      val dot = fsmToDot.compileToDot (m.getMachines.get (0))
      new PrintWriter ("test-out/CoffeeMachine.dot"):
        write (dot)
        close
    }
  }
