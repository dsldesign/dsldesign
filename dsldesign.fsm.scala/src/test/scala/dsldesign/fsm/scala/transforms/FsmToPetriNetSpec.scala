// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.transforms

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil

import dsldesign.fsm
import dsldesign.petrinet
import dsldesign.scala.emf

class FsmToPetriNetSpec extends 
  org.scalatest.freespec.AnyFreeSpec, 
  org.scalatest.matchers.should.Matchers:

  // Initialize EMF

  fsm.FsmPackage.eINSTANCE.eClass
  petrinet.PetrinetPackage.eINSTANCE.eClass

  // Set up fixtures. Note: imperative tests destroy fixtures

  val ids = List ("test-00", "test-01", "test-02", "test-03", "test-04", 
                  "test-05", "test-06", "test-07")
  val N = ids.size - 1
  val fnames = ids.map { t => s"../dsldesign.fsm/test-files/$t.xmi" }
  val roots: List[fsm.Model] = fnames.map { emf.loadFromXMI[fsm.Model] }

  final case class Fixture (root: fsm.Model, id: String)

  // A convenience factory
  def fixture (n: Int) = Fixture (
    root = EcoreUtil.copy (roots (n)).asInstanceOf[fsm.Model],
    id = ids (n))

  // Helper methods

  def validates (root: EObject): Boolean =
    emf.validate (root, false)
      .map[Boolean] { m => m.foreach[Unit] { s => info (s"\t$s") }; false }
      .getOrElse (true)

  // Tests

  "FsmToPetriNet" - {

    s"give results that validate in the new metamodel" in {
      val in = (0 to N)
        .map { fixture }
        .filter { m => validates (m.root) }
      val out = in.map { m => FsmToPetriNet.run (m.root) }
      for o <- out 
        do validates (o) shouldBe true
    }

    s"save results of each transformations into new petrinet xmi format" in {
      val in = (0 to N)
        .map { fixture } 
        .filter { m => validates (m.root) }
      val out = in.map { f => (f.id, FsmToPetriNet.run (f.root)) }
      for (id, root) <- out 
        do emf.saveAsXMI (s"test-out/$id.pn.xmi") (root)

      // Let's see whether they load without errors
      val reloaded = out
        .map { m => emf.loadFromXMI[petrinet.Model](s"test-out/${m._1}.pn.xmi") }
      for m <- reloaded 
        do validates (m) shouldBe true
    }
  }
