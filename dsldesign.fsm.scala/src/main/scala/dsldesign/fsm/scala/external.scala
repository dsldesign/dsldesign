// (c) dsldesign, wasowski, tberger
// Run FSM interpreter with a CLI frontend; load and parse outside Eclipse.
package dsldesign.fsm.scala.external

import dsldesign.fsm.{FsmPackage, Model}
import dsldesign.fsm.scala.*
import dsldesign.scala.emf.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

@main def main =
  // Change input file name of your state machine model here;
  // path is relative to project root
  val instanceFileName = "../dsldesign.fsm/test-files/CoffeeMachine.xmi"

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE
    .getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // Register our meta-model package for abstract syntax
  FsmPackage.eINSTANCE.eClass

  // Load the file
  val uri = URI.createURI (instanceFileName)
  var resource = ResourceSetImpl ().getResource (uri, true);

  // The call to get(0) below gives you the first model root.
  // If you open a directory instead of a file,
  // you can iterate over all models in it,
  // by changing 0 to a suitable index
  val model = resource.getContents.get (0).asInstanceOf[Model]

  run (model)
