// (c) dsldesign, wasowski, tberger
// A pure altarnative to meta-modeling in functional style.
// Easy to construct with a parser.
package dsldesign.fsm.scala.adt

sealed abstract trait ModelElement
abstract trait NamedElement:
  val name: String

final case class Model (
  name: String, 
  machines: List[FiniteStateMachine]
) extends ModelElement, NamedElement

type StateName = String

final case class FiniteStateMachine (
  name: String,
  states: List[StateName],
  transitions: Map[StateName, List[Transition]],
  initial: String
) extends ModelElement, NamedElement

final case class Transition (
  target: StateName,
  input: String,
  output: String = ""
) extends ModelElement
