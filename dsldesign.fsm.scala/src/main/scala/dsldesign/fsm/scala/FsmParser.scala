// (c) dsldesign, wasowski, tberger
// A parser for Fsm implemented using Parboiled2
// An alternative to the xtext parser defined in
// dsldesign.fsm.xtext/src/main/java/dsldesign/fsm/xtext/Fsm.xtext
package dsldesign.fsm.scala

import scala.language.implicitConversions
import org.parboiled2.*
import dsldesign.fsm.scala.adt

// Because of a bug
// (https://github.com/sirthias/parboiled2/issues/356) in porting
// parboiled to Scala 3, all actions have to temporarily be lambda
// functions (until fixed). I had to insert an explicit η-abstraction
// to make this compile in Scala 3 with parboiled 2.4.0. For this
// reason the code in the book figures is a bit cleaner than in
// reality.  This used to work with Scala 2. 
//
// The problematic lines are marked "// *"

case class FsmParser (val input: ParserInput) extends Parser:

  // Non-terminals, starting with the Start symbol

  def model: Rule1[adt.Model] =
    rule { machineBlock.* ~ EOI 
      ~> (machines => Model (machines)) } // *

  def machineBlock: Rule1[adt.FiniteStateMachine] =
    rule {
      "machine" ~ EString ~ BEGIN ~
        stateBlock.* ~
        initialDeclaration ~
        stateBlock.* ~
      END 
        ~> ((name, s1, ini, s2) => FiniteStateMachine (name, s1, ini, s2)) // *
    }

  def initialDeclaration: Rule1[String] =
    rule { "initial" ~ EString }

  def stateBlock: Rule1[StateTr] =
    rule {
      "state" ~ EString ~ ( BEGIN ~
        transition.* ~ END ).? 
        ~> ((state, transitions) => StateTransitions (state, transitions)) // *
    }

  def transition: Rule1[adt.Transition] =
    rule { inputClause ~ outputClause.? ~ targetClause 
      ~> ((input, output, target) => Transition (input, output, target)) } // *

  def inputClause: Rule1[String] =
    rule { "on" ~ "input".? ~ EString }

  def outputClause: Rule1[String] =
    rule { "output" ~ EString ~ "and" }

  def targetClause: Rule1[String] =
    rule { "go" ~ "to" ~ EString }

  def EString: Rule1[String] =
    rule { ID | STRING }


  // Terminals and whitespace ("lexing")

  def STRING: Rule1[String] =
    rule { WS.? ~ '"' ~ capture ((! '"'  ~ ANY).*) ~ '"' ~ WS.? }

  val IDFirst: CharPredicate = CharPredicate.Alpha ++ "_"
  val IDSuffix: CharPredicate = CharPredicate.AlphaNum ++ "_"

  def ID: Rule1[String] =
    rule { WS.? ~ capture (IDFirst ~ IDSuffix.*) ~ WS.? }

  def BEGIN =
    rule { WS.? ~ '[' ~ WS.? }
  def END =
    rule { WS.? ~ ']' ~ WS.? }

  given Conversion[String, Rule0] with
    def apply (s: String): Rule0 = 
      rule { WS.? ~ str (s) ~ WS }

  def WS: Rule0 =
    rule { anyOf (" \n\t").+ }


  // Semantic actions (gathered here to keep the grammar clean)

  val Model: Seq[adt.FiniteStateMachine] => adt.Model =
    machines => adt.Model ("", machines.toList)

  type StateTr = (String, List[adt.Transition])

  val FiniteStateMachine:
    (String, Seq[StateTr], String, Seq[StateTr]) => adt.FiniteStateMachine =
      (name, s1, ini, s2) =>
        adt.FiniteStateMachine (
          name, (s1++s2).map (_._1).toList, (s1++s2).toMap, ini)

  val StateTransitions: (String, Option[Seq[adt.Transition]]) => StateTr =
      _ -> _.getOrElse (Nil).toList

  val Transition: (String, Option[String], String) => adt.Transition =
    (input, output, target) =>
      adt.Transition (target, input, output getOrElse "")
