// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala

import scala.io
import scala.jdk.CollectionConverters.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import dsldesign.fsm
import dsldesign.scala.emf.*

// An interpreter for FSM models with a single machine
def run (m: fsm.Model): Unit =
  require (m != null && m.getMachines.size == 1)
  repl (m.getMachines.get (0).getInitial)


// A repl for FSM that reads an input from the user, evaluates it
// to get a new state, prints an output (if present), and loops
// again on the new state.  REPL = Read-Evaluate-Print-Loop
def repl (s: fsm.State): Unit =
  val inputs = s.getLeavingTransitions.asScala
                .map { _.getInput } 
                .mkString (", ")
  print (s"\nMachine is in state: ${s.getName}. Input [$inputs]? ")
  val input = io.StdIn.readLine

  eval (s) (input) match
    case Some (s1 -> Some (output)) =>
      repl (s1) before { println (s"Machine outputs: $output") }
    case Some (s1 -> None) =>
      repl (s1)
    case None =>
      repl (s) before { println ("Invalid input!") }


/** An evaluator for a given state. Returns None if there is no transition
  * labeled 'input', or (Some of) a pair of the successor state and an output
  * message.  The output message is optional, because we admit silent.
  * transitions.
  *
  * @param s presently active state
  * @param input the input message (action/event) to be send to the machine
  * @return result of finding and firing a suitable transition.
  */
def eval (s: fsm.State) (input: String): Option[(fsm.State, Option[String])] =
  s.getLeavingTransitions.asScala
   .find { tran => tran.getInput == input }
   .map { tran => (tran.getTarget, Option (tran.getOutput)) }

// The main function to run the constraints checking example
@main def main =
  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", XMIResourceFactoryImpl ())

  // register the package magic (impure)
  dsldesign.fsm.FsmPackage.eINSTANCE.eClass

  // load the XMI file (change file name here)
  val uri: URI = URI.createURI ("../dsldesign.fsm/test-files/test-02.xmi")
  val resource: Resource = ResourceSetImpl ().getResource (uri, true)

  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content: Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).asScala

  if (content.forall { eo => constraints.invariants.forall (_ check eo) })
    println ("All constraints are satisfied!")
  else
    println ("Some constraint is violated!")
