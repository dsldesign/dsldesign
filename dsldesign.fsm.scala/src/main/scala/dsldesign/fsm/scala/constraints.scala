// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.constraints

import org.eclipse.emf.ecore.EObject
import scala.jdk.CollectionConverters.*
import dsldesign.scala.emf.*
import dsldesign.fsm.*

// Constraints from the running example in Chapter 5

// C1: All machines must have distinct names
val C1 = inv[Model] { M =>
  M.getMachines.asScala.forall { m1 =>
    M.getMachines.asScala.forall { m2 =>
      m1 != m2 implies m1.getName != m2.getName
    }
  }
}

// C2: all states within the same machine must have distinct names
// ∀m ∀s1 ∀s2 ∀n1 ∀n2.
//   s1 != s2 ∧ states(m,s1) ∧ states(m,s2) ∧ name (s1,n1) ∧ name (s2,n2) → n1!=n2
val C2 = inv[FiniteStateMachine] { m =>
  m.getStates.asScala.forall { s1 =>
    m.getStates.asScala.forall { s2 =>
      s1 != s2 implies s1.getName != s2.getName
    }
  }
}

// Even shorter formulations of C2 using dsldesign.scala and wildcards

val C2a = inv[FiniteStateMachine] {
  _.getStates.asScala.forall { (s1: State, s2: State) =>
      s1 != s2 implies s1.getName != s2.getName }
}

val C2b = inv[FiniteStateMachine] {
  _.getStates.asScala.forAllDifferent { _.getName != _.getName } }

// My favourite formulation of C2 in Scala

val C2_GOOD = inv[FiniteStateMachine] { self =>
self.getStates.asScala.forAllDifferent { (s1,s2) => s1.getName != s2.getName } }

//  An unacceptable formulation of C2

def C2_BAD (m: FiniteStateMachine): Boolean = {

  var it1 = m.getStates.iterator

  while it1.hasNext do
    var s1: State = it1.next
    var it2 = m.getStates.iterator
    while it2.hasNext do
      var s2: State = it2.next
      if s1 != s2 && s1.getName == s2.getName then
          return false

  return true
}

// C3: For each state machine m, the state designated as the initial state of
// m is also a member of the collection of states contained in M .
// ∀m.∀s. initial (m,s) → states (m,s)
val C3 = inv[FiniteStateMachine] { m => m.getStates.contains (m.getInitial) }

// C4: Transitions cannot cross machine boundaries
// ∀t ∀s1 ∀s2 ∀m source(t,s1) ∧ target(t,s2) ∧ machine(s1,m) → machine(s2,m)
val C4 = inv[Transition] { t => t.getSource.getMachine == t.getTarget.getMachine }

// C5: Each state must be reachable from the initial state in each state machine.
// Since it is a connectivity property we implement it as an algorithm, not
// a declarative constraint.
val C5 = inv[State] { t => reachable (t.getMachine.getInitial, t) }

def reachable (s1: State, s2: State): Boolean =
  def succ (s: State): Seq[State] =
    s.getLeavingTransitions.asScala.map { _.getTarget }.toSeq

  def BFS (toSee: Set[State], seen: Set[State]): Set[State] =
    if toSee.isEmpty then seen
    else
      val seen1 = seen union toSee
      BFS (toSee.flatMap {succ _}.diff (seen1), seen1)

  BFS (Set(s1), Set()) contains s2

// C6: The state machine is deterministic, so in each state each outgoing
// transition has a different label (not very efficient but good enough for a
// small example).
//
// We are using the "implies" operator provided by dsldesign.scala.emf

val C6 = inv[State] { self =>
  self.getLeavingTransitions.asScala.forall { t1 =>
    self.getLeavingTransitions.asScala.forall { t2 =>
      t1 != t2  implies  t1.getInput != t2.getInput
    }
  }
}


// More examples of constraints (not used in the chapter directly)

// There must be a loop transition labeled idle for each state
// (this one is kept separate, so we can use it in testing trafos)

val idle = inv[State] { self =>
    self.getLeavingTransitions.asScala.exists { t =>
      t.getInput == "idle" && t.getTarget == self }
  }


// An aggregation of all constraints in a single list for easy checking

val invariants: List[Constraint] = List (

  inv[FiniteStateMachine] { self => self.getStates.contains (self.getInitial) },

  // Name cannot be empty for any named element.
  // It is already not null by meta-model constraints

  inv[NamedElement] { !_.getName.isEmpty },

  C1,
  C2,
  C3,
  C4,
  C5,
  C6,

  idle, //include the idle constraint


  // For each transition, target and source states are in the same state machine
  inv[Transition] { self =>
    self.getSource.getMachine == self.getTarget.getMachine
  },


  // Evaluation version: maximum 40 states in the model
  // No sharing of states is guaranteed by containment
  inv[Model] { self =>
    self.getMachines.asScala.foldLeft(0) { (sum,m) => sum + m.getStates.size } <= 40
  }

)
