// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.internal.deep.coffeeMachineExplained

import scala.language.postfixOps

import dsldesign.fsm
import dsldesign.fsm.scala.internal.deep.*

// An example of the Coffee Machine model written in Scala, using the internal
// dsl syntax from dsldesign.fsm.scala.internal.Deep, with commentary.

// The following partial expressions of the above example explain how this is
// built (See also internal.scala).  t4 (below) is also written following Java
// syntactic convention with parentheses and navigation, to give you an idea
// how the compiler sees this model.
//
// The type of a partial term, is named after the possible next language
// elements

val t0: state.type = state

val t1: INITIAL_OR_STATE_OR_END =
    state machine "coffeeMachine"

val t2: fsm.Model =
  ( state machine "coffeMachine"
    end )

val t3: INPUT_OR_NEXT_STATE = (
  state machine "coffeeMachine"
    initial "initial"
)

val t4: fsm.Model = (
  state machine "coffeeMachine"
    initial "initial"
  end
)

// The above example made more explicit syntactically
val t4withDots =
  state.machine ("coffeeMachine").initial ("initial").end

/***
 * Notes for the above
 *
 * - state (first ocurence) is an object in dsldesign.fsm.scala.internal
 *   machine is a unary method taking a string
 * - state (second occurence) is a unary method taking a string.  It is a
 *   method of the type produce by machine
 * - end is a unary method taking a single object parameter.
 * - state (the third occurence) is a singleton object (actually the same
 *   object as in the first occurence)
 * - end is a unary method taking a single object parameter (a different but
 *   similar to the method providing the other end keyword).
 * - machine (the last occurence) is a singleton object serving the role of a
 *   keyword in the internalDSL
 */

/* We could write it like that also, even with dots, so that it appears more
 * DSLish (Common style in Java and C#) :
 **/

val t4laidOut =
  state .machine ("coffeeMachine")
    .initial ("initial")
    .end

/**
 * Although in a language where dots and parentheses are unavoidable, we would
 * likely prefer to code the last state as a nullary method or a field
 * obtaining something similar to:
 *
 * state .machine ("coffeeMachine")
 *   .state ("initial")
 * .end .state
 */

val t5: OUTPUT_OR_TARGET = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"
)

val t6: TARGET = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin" output "what drink do you want?"
)

val t7: TARGET = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin" output "what drink do you want?"
)

val t8: INPUT_OR_NEXT_STATE = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"output "what drink do you want?" target "selection"
)

val t9: OUTPUT_OR_TARGET = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break"
)

val t10: TARGET = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"
)

val t11: INPUT_OR_NEXT_STATE = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"       target  "broken"
)

val t12: INPUT_OR_NEXT_STATE = (
  state machine "coffeeMachine"
    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"       target  "broken"
)

val t13: fsm.Model = (

  state machine "coffeeMachine"

    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"       target "broken"

  end
)

val t15 = (

  state machine "coffeeMachine"

    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"       target  "broken"

    state "initial"
)

val t16: fsm.Model =
(state machine "coffeeMachine"

    initial "initial"
      input "coin"  output "what drink do you want?" target "selection"
      input "break" output "machine is broken"       target  "broken"

    state "selection"
      input "tea"     output "serving tea"                target "making tea"
      input "coffee"  output "serving coffee"             target "making coffee"
      input "timeout" output "coin returned; insert coin" target "initial"
      input "break"   output "machine is broken!"         target "broken"

end)
