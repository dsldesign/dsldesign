// (c) dsldesign, wasowski, tberger
// The FSM language implemented as a shallow DSL embedded in Scala.  Only one
// machine per model.
package dsldesign.fsm.scala.internal.shallow

type Step = String => (Option[String], State)

val state: State = new State:
  def step: Step = 
    { (input: String) => (Some ("Unknown input msg!"), state) } 


trait State:
  /** A State object embeds a function 'step', its essential element that
    * represents the meaning of the state -- the transition from an input to an
    * output and a target state. We implement this function (abstract here)
    * differently for each actual state, see below under 'target'.
    */
  def step: Step

  /** Our DSL has "partial operators". Input by itself is unable to create a
    * state transition. It needs an output (optional) and a target (mandatory).
    * Thus we "cheat" slightly, and create a local "deep" representation, a
    * suspended transition. Once the target is available, we immediately provide
    * the semantics for triaging the transition.
    */
  def input (event: String): Suspended =
    Suspended (source=this, event=event, output=None)


case class Suspended (source: State, event: String, output: Option[String]):

  def output (o: String): Suspended =
    Suspended (source, event, Some (o))

  /** Target is the operator that is "ready" to create a new State, as all the
    * information is now available. It compiles a transition to a State, so a
    * function from input to an output and a target state.
    */
  def target (t: => State): State = new State:
    def step: Step = input =>
      if input == event 
        then (output, t)
        else source.step (input)
