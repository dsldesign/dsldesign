// (c) dsldesign, wasowski, tberger
// The FSM language implemented as an internal DSL in Scala.  
// Only one machine per model, for simplicity.
package dsldesign.fsm.scala.internal.deep

import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*
import dsldesign.fsm

/** Very simple AST algebraic data types (ADT) for internal representation of
  * the parsed model in Scala. In the last phase of execution we will transform
  * this to EMF types, so that we gain compatibility with other possible parts
  * of the tool chain that may be implemented following the external DSL route.
  */
final case class ModelRep (
  name: String,
  states: List[String] = Nil,
  tran: List[TranRep] = Nil,
  initial: Option[String] = None)

final case class TranRep (
  source: String,
  input: Option[String] = None,
  output: Option[String] = None,
  target: Option[String] = None)

/** Below we capture the syntax of the internal DSLs. The keywords are captured
  * either by singleton objects or methods.  An object can introduce the
  * keywords that are allowed directly to follow it by implementing the
  * suitably named methods. Each method returns an object of a new class that
  * defines what keywords are allowed to follow.
  */

/** Introduce the opening keywords "state machine".  Note that the other use
  * of "state", for defining a new state is not implemented using this
  * singleton, but using a method on "EXPECT_INITIAL_OR_STATE_OR_END.
  */
case object state:
  def machine (name: String) =
    INITIAL_OR_STATE_OR_END (ModelRep (name))


/** Inside a machine context we allow opening a state definition (the "state"
  * method and the "initial" method for initial states) and a closing of a
  * machine with an "end" keyword.
  */
final case class INITIAL_OR_STATE_OR_END (machine: ModelRep):

  /** Open a new state object, with an "initial" keyword, so representing the
    * initial state.
    */
  def initial (src: String) =
    val model1 = machine.copy (initial = Some (src), states = src ::machine.states)
    INPUT_OR_NEXT_STATE (model1, src)

  /** Open a new state object, with a "state" keyword, after this only "input"
    * is a valid input, or we are seeing a new state with "state" or "initial"
    * (or "end" to close the model).
    */
  def state (src: String) =
    val model1 = machine.copy (states = src ::machine.states)
    INPUT_OR_NEXT_STATE (model1, src)

  /** Close the current machine object, with "end" and translate the current
    * partial model representation to an Ecore instance.
    */
  def end: fsm.Model =
    modelRep2Model (machine)


/** Detect a new transition (input) or end of the state definition */
final case class INPUT_OR_NEXT_STATE (machine: ModelRep, src: String):

  /** Parse a new transition definition */
  def input (input: String) =
    val tran = TranRep (source = src, input = Some (input))
    OUTPUT_OR_TARGET (machine, src, tran)

  /** End of the state definition, start a new state. We delegate to
    * the machine level and the function already implemented there.
    */
  def state (name: String) =
    INITIAL_OR_STATE_OR_END (machine).state (name)

  /** End of the state definition, start a new initial state. We delegete
    * the machine level and the function already implemented there.
    */
  def initial (name: String) =
    INITIAL_OR_STATE_OR_END (machine).initial (name)

  /** End the state machine definition. We delegate to the machine level. */
  def end: fsm.Model =
    INITIAL_OR_STATE_OR_END (machine).end


/** Detect the target state phrase */
sealed class TARGET (machine: ModelRep, src: String, tran: TranRep):

  /** Detect "and go" and move to an objecct that awaits for "to".
    * We could use inheritance here to merge it with EXPECT_OUTPUT_OR_AND.
    */
  def target (name: String) =
    val tran1 = tran.copy (target = Some (name))
    val machine1 = machine.copy (tran = tran1 ::machine.tran)
    INPUT_OR_NEXT_STATE (machine1, src)


/** Detect an optional output actiona or a target state */
final case class OUTPUT_OR_TARGET (machine: ModelRep, src: String, tran: TranRep)
  extends TARGET (machine, src, tran):

  /** Record the output of a transition and move to the target state.  */
  def output (output: String) =
    val tran1 = tran.copy (output = Some(output))
    TARGET (machine, src, tran1)




// A transformation from our ADT representation to Ecore FSM instance.
// Imperative (as Ecore's design is).
private[deep] def modelRep2Model (m: ModelRep) : fsm.Model =
  val s2tMap = m.tran.map { t => t.source -> t }.toMap

  def stateRep2State (s: String) : fsm.State =
    val S = fsm.FsmFactory.eINSTANCE.createState
    S.setName (s)
    return S

  val SS = m.states.map (s => s -> stateRep2State (s)).toMap

  def tranRep2Transition (t: TranRep): fsm.Transition =
    val T = fsm.FsmFactory.eINSTANCE.createTransition
    for i <- t.input do  T.setInput (i)
    for o <- t.output do T.setOutput (o)
    for s <- t.target do T.setTarget (SS (s))
    return T

  for t <- m.tran 
  do SS(t.source).getLeavingTransitions.add (tranRep2Transition (t))

  val M = fsm.FsmFactory.eINSTANCE.createModel
  M.setName (m.name)
  val FSM = fsm.FsmFactory.eINSTANCE.createFiniteStateMachine
  M.getMachines.add (FSM)
  FSM.getStates.addAll (SS.asJava.values)
  for s <- m.initial do FSM.setInitial (SS(s))
  return M
