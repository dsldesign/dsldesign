// (c) dsldesign, wasowski, tberger
// An example model, CoffeeState, in the fsm language, the variant using
// shallow embedding in Scala (an internal dSL).
package dsldesign.fsm.scala.internal.shallow.coffeeMachine

import dsldesign.fsm.scala.internal.shallow.*

lazy val initial: State = (state
  input "coin"  output "what drink do you want?" target selection
  input "idle"                                   target initial
  input "break" output  "machine is broken"      target deadlock
)

lazy val selection: State = (state
  input "tea"     output "serving tea"                target makingTea
  input "coffee"  output "serving coffee"             target makingCoffee
  input "timeout" output "coin returned; insert coin" target initial
  input "break"   output "machine is broken!"         target deadlock
)

lazy val makingCoffee: State = (state
  input "done"    output "coffee served. Enjoy!"      target initial
  input "break"   output "machine is broken!"         target deadlock
)

lazy val makingTea: State = (state
  input "done"    output "tea served. Enjoy!"         target initial
  input "break"   output "machine is broken!"         target deadlock
)

lazy val deadlock: State = (state)
