// (c) dsldesign, wasowski, tberger
// An example model, CoffeeMachine, in the fsm language, the variant using
// deep embedding in Scala (an internal dSL).
package dsldesign.fsm.scala.internal.deep.coffeeMachine

import scala.language.postfixOps
import dsldesign.fsm.scala.internal.deep.*

val m = (state machine "coffeeMachine"

  initial "initial"
    input "coin"    output "what drink do you want?"    target "selection"
    input "idle"                                        target "initial"
    input "break"   output "machine is broken"          target "deadlock"

  state "selection"
    input "tea"     output "serving tea"                target "making tea"
    input "coffee"  output "serving coffee"             target "making coffee"
    input "timeout" output "coin returned; insert coin" target "initial"
    input "break"   output "machine is broken!"         target "deadlock"

  state "making coffee"
    input "done"    output "coffee served. Enjoy!"      target "initial"
    input "break"   output "machine is broken!"         target "deadlock"

  state "making tea"
    input "done"    output "tea served. Enjoy!"         target "initial"
    input "break"   output "machine is broken!"         target "deadlock"

  state "deadlock"

end)

// An example of a main function to allow running the models
@main def main = 
  dsldesign.fsm.scala.run (m)
