// (c) dsldesign, wasowski, tberger
// A generator of dot files.  Compile the results with
// dot -Tpdf CoffeeMachine.dot -o CoffeeMachine.pdf
package dsldesign.fsm.scala.transforms.fsmToDot

import scala.jdk.CollectionConverters.*

import dsldesign.fsm.*
import dsldesign.scala.emf.*

def fmtTransitionDot (t: Transition): String =
  s"""|  "${t.getSource.getName}" -> "${t.getTarget.getName}"
      |      [label="${t.getInput} / ${t.getOutput}"];"""

def fmtStateDot (s: State): String =
  s.getLeavingTransitions.asScala
    .map (fmtTransitionDot) 
    .mkString ("\n")

def compileToDot (it: FiniteStateMachine): String =
  s"""digraph "${it.getName}" {
     |  _init -> "${it.getInitial.getName}";
        ${it.getStates.asScala.map (fmtStateDot).mkString ("\n")}
     |  ${it.getInitial.getName} [shape=doublecircle];
     |  _init [shape=point];
     |}
     |""".stripMargin
