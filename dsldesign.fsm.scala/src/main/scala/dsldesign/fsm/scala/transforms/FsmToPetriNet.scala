// (c) dsldesign, wasowski, tberger
// An in-place transformation implemented directly in Scala (no special
// frameworks).  Translates a finite state machine to a Petri net.  There is
// presently no runner for this example.
package dsldesign.fsm.scala.transforms

import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*
import dsldesign.{fsm, petrinet}

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import dsldesign.fsm.{FsmPackage, Model}

object FsmToPetriNet extends CopyingTrafo[fsm.Model, petrinet.Model]:
  val pFactory = petrinet.PetrinetFactory.eINSTANCE

  // We inject some queries over States
  extension (self: fsm.State)
    def getInitialTokenCount = if (self.getMachine.getInitial == self) 1 else 0
    def isEndState = self.getLeavingTransitions.isEmpty

  def convertState (self: fsm.State): petrinet.Place = 
    pFactory.createPlace before { p =>
      p.setName(self.getName)
      p.setTokenNo(self.getInitialTokenCount)
    }

  def convertTransition (places: List[petrinet.Place]) (self: fsm.Transition)
    : petrinet.Transition =
    pFactory.createTransition before { pnt =>
      pnt.setInput(self.getInput)
      pnt.getFromPlace.addAll(places.filter(_.getName == self.getSource.getName).asJava)
      pnt.getToPlace.addAll(places.filter(_.getName == self.getTarget.getName).asJava)
    }

  def convertEndState2RemTrans (places: List[petrinet.Place]) (self: fsm.State)
    : petrinet.Transition =
    pFactory.createTransition before { pnt =>
      pnt.setInput("")
      pnt.getFromPlace.add(places.find(_.getName == self.getName).get)
  }

  def convertStateMachine (self: fsm.FiniteStateMachine): petrinet.PetriNet =
    pFactory.createPetriNet before { pn =>
      pn.setName(self.getName)
      val places = self.getStates.asScala.toList.map(convertState)
      pn.getPlaces.addAll(places.asJava)
      pn.getTransitions.addAll(
        self.getStates.asScala
        .flatMap(_.getLeavingTransitions.asScala.toList)
        .map(convertTransition(places))
        .asJava)

      // for each end state generate a transition that quashes a token
      pn.getTransitions.addAll (
        self.getStates.asScala
        .filter(_.isEndState)
        .map(convertEndState2RemTrans (places))
        .asJava)
    }

  override def run (self: fsm.Model): petrinet.Model =
    pFactory.createModel before {
      _.getPetrinets.addAll(self.getMachines.asScala.map(convertStateMachine _).asJava) }


// The main function to run the trafo
@main def fsmToPetriNetMain =
  // Register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // Register the package magic (impure)
  FsmPackage.eINSTANCE.eClass

  // Load the fsm model
  val model: Model = loadFromXMI ("dsldesign.fsm/test-files/CoffeeMachine.xmi")

  // We run the transformation
  val petrinetModel = FsmToPetriNet.run (model)

  // Save the petrinet model
  val output = "dsldesign.petrinet/test-files/CoffeeMachine-transformed-by-Scala.xmi"
  saveAsXMI (output) (petrinetModel)
