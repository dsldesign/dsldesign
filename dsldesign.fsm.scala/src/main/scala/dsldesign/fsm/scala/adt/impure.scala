// (c) dsldesign, wasowski, tberger
// Use an ADT instead of a meta-model/Ecore
// An impure implementation with case classes, Java style.
package dsldesign.fsm.scala.adt.impure

sealed abstract trait ModelElement
abstract trait NamedElement:
  val name: String

final case class Model (
  name: String, 
  machines: List[FiniteStateMachine]
) extends ModelElement, NamedElement

final case class State (
  name: String,
  var leavingTransitions: List[Transition]
) extends ModelElement, NamedElement

final case class FiniteStateMachine (
  name: String,
  states: List[State],
  initial: State
) extends ModelElement, NamedElement

case class Transition (
  target: State,
  source: State,
  input: String,
  output: String = ""
) extends ModelElement
