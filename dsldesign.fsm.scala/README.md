To obtain the coverage reports used in the book chapter use the
following command in the project root directory:

```bash
./gradlew :dsldesign.fsm.scala:reportScoverage
```

Then navigate to `build/reports/` with a web browser.
