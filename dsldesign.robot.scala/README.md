This project has been disabled (disconnected from the main build) and
it is not presently maintained.  It demonstrates implementing the
.robot DSL using ROS1 and ros_java. However ROS1 and ros_java are no
longer maintained, and it became to difficult to keep this project
secure and alive.

Please see other dsldesign.robot* projects for an implementation using
ROS2 and Python.

Perhaps one day, this could be resurected using ROS2 and
https://github.com/ros2-java/ros2_java .
