// (c) dsldesign, wasowski, berger
// This code is purposefully not made purely functional, to follow
// the programming style of Java ROS as closely as possible

package dsldesign.robot.scala

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

import org.ros.message.MessageListener
import org.ros.namespace.GraphName
import org.ros.node.AbstractNodeMain
import org.ros.node.ConnectedNode

import sensor_msgs.LaserScan


class Controller extends AbstractNodeMain {

  var thymio: Thymio = null

  override def getDefaultNodeName(): GraphName =
    GraphName.of ("dsldesign/robot/scala/thymio_controller")

  override def onStart(cn: ConnectedNode): Unit = {
    Thread.sleep (1200)
    this.thymio = new Thymio (cn)

    var listener = new MessageListener[LaserScan] {
      var lock: Lock = new ReentrantLock
      override def onNewMessage (msg: LaserScan): Unit =
        if (msg.getIntensities.sum > 0.09 && lock.tryLock) try {
          thymio.stop
          thymio.backOff
          thymio.randomRotation
          thymio.engage
        } finally this.lock.unlock
    }
    this.thymio.getProximityTopic addMessageListener listener
    this.thymio.engage
  }
}

object RandomWalkExample extends App {
  org.ros.RosRun.main(Array("dsldesign.robot.scala.Controller"))
}
