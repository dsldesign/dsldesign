// (c) dsldesign, wasowski, tberger

// This code is purposefully not made purely functional, to follow
// the programming style of Java ROS as closely as possible

// Apparently this is not a great architectural design.  The interpreter
// should be implemented independently of RosJava and the interfaced
// (this is the recommended style in ROS).  Our Python/ROS2 interpreter
// in dsldesign.robot.turtlebot3 is better separated.

// Limitations of this simple implementation:
// - only deepest nested transitions are active;
//   switch to a stack of lists to support the full language
// - execution of state scripts and state sequences is non-premptive
// - durations and expressions are not implemented
// - odometry, planning, and docking are not implemented.
// - ROS and the interpreter are only slightly separated
//   (but we are moving in a good direction)
// - Scala's concurrency is not used (Futures or Actors should be used)

package dsldesign.robot.scala.interpreter

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

import scala.jdk.CollectionConverters._
import scala.language.implicitConversions

import org.ros.message.MessageListener
import org.ros.namespace.GraphName
import org.ros.node.ConnectedNode
import org.ros.node.Node
import org.ros.node.NodeMain

import dsldesign.robot.AcMove
import dsldesign.robot.AcTurn
import dsldesign.robot.Action
import dsldesign.robot.Event
import dsldesign.robot.Event.EV_OBSTACLE
import dsldesign.robot.Mode
import dsldesign.robot.Reaction
import dsldesign.robot.RobotFactory
import dsldesign.robot.scala.Thymio
import dsldesign.scala2.emf.EMFConvenienceOps
import sensor_msgs.LaserScan

case class State(thymio: Thymio, activeTr: Map[Event, Reaction], root: Mode) {

  def engage: State = this before { _.thymio.engage }
  def backOff: State = this before { _.thymio.backOff }
  def randomRotation = this before { _.thymio.randomRotation }
  def stop: State = this before { _.thymio.stop }

  def setRoot (m: Mode) :State = this copy (root = m)

  def activate: State = {

    println (s"Activating mode: [${root.getName}]");

    val s1 = root.getActions.asScala.foldLeft (this) (_ executeAc _)
    val newTr = root.getReactions.asScala.map { r => (r.getTrigger, r) }.toMap
    val s2 = s1.copy(activeTr = newTr)

    // constraint: there should be initial state at each level, unless this is a leaf level

    val s3 = root.getModes.asScala find {_.isInitial} match {
      case Some (m) => s2.setRoot(m).activate
      case None => s2
    }

    if (root.getContinuation == null) s3
    else s3.setRoot (root.getContinuation).activate
  }

  def executeAc (a: Action): State = {
    a match {
      case a: AcMove => if (a.isForward) engage else backOff
      case a: AcTurn => randomRotation
      case _ => ???
    }
  }

  def processEvent (e: Event): State = e match {
    case EV_OBSTACLE => setRoot(activeTr (EV_OBSTACLE).getTarget).activate
    case _ => ???
  }

}

class Interpreter(root: Mode) extends NodeMain {

  var lock: Lock = new ReentrantLock

  override def getDefaultNodeName(): GraphName =
    GraphName.of("dsldesign/robot/scala/interpreter")

  override def onStart(cn: ConnectedNode): Unit = {
    Thread.sleep (1000)
    var state = State(new dsldesign.robot.scala.Thymio(cn), Map[Event, Reaction](), root)

    var listener = new MessageListener[LaserScan] {
      override def onNewMessage(msg: LaserScan): Unit =
        // obstacle event
        if (msg.getIntensities.sum > 0.09 && lock.tryLock)
          try state = state.processEvent (EV_OBSTACLE)
          finally lock.unlock
    }

    if (lock.tryLock) try {
      state.thymio.getProximityTopic.addMessageListener (listener)
      state = state.activate
    } finally lock.unlock
  }

  override def onError(n: Node, t: Throwable): Unit = ()
  override def onShutdown(n: Node): Unit = ()  // would be better to stop the robot
  override def onShutdownComplete(n: Node): Unit = ()

}

object Interpreter {

  def apply(root: Mode): Interpreter = new Interpreter(root)

}

