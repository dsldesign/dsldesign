package dsldesign.robot.scala.interpreter

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.ros.node.DefaultNodeMainExecutor
import org.ros.node.NodeConfiguration
import org.ros.node.NodeMainExecutor

object Main extends App {

	// Change input file name of your state machine model here;
	// path is relative to project root
	val instanceFileName = "../dsldesign.robot/test-files/random-walk.xmi"

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
  getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // Register our meta-model package for abstract syntax
  dsldesign.robot.RobotPackage.eINSTANCE.eClass

  // we are loading our file here
  val uri = URI.createURI (instanceFileName)
  var resource = new ResourceSetImpl().getResource (uri, true);

  // The call to get(0) below gives you the first model root.
  // If you open a directory instead of a file,
  // you can iterate over all models in it,
  // by changing 0 to a suitable index
  val model = resource.getContents.get (0).asInstanceOf[dsldesign.robot.Mode]

  var nodeMainExecutor :NodeMainExecutor = DefaultNodeMainExecutor.newDefault
  nodeMainExecutor.execute(Interpreter (model), NodeConfiguration.newPrivate)
}
