// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.xtext.generator;

import dsldesign.fsm.FiniteStateMachine;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;


public class FsmGenerator extends AbstractGenerator {

  private String capitalize (String s) {
    return s.substring (0,1).toUpperCase () + s.substring (1);
  }

  public void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext ctx) { 
    TreeIterator<EObject> it = resource.getAllContents ();

    while (it.hasNext())  {
      EObject m = it.next ();
      if ( m instanceof FiniteStateMachine ) {
        FiniteStateMachine fsm = (FiniteStateMachine)m;
        fsa.generateFile (capitalize(fsm.getName()) + ".java", 
          dsldesign.fsm.scala.transforms.fsmToJava.fsmToJava$package.compileToJava (fsm));
        fsa.generateFile (capitalize(fsm.getName()) + ".dot", 
          dsldesign.fsm.scala.transforms.fsmToDot.fsmToDot$package.compileToDot (fsm));
      }
    }

  }

  // FIXME: one day when we have more time, translate the following Xtend code to Scala, 
  // to automatically call graphviz (not clear how many readers install graphviz anyways)
  //
  // import org.eclipse.core.resources.ResourcesPlugin 
  //	// execute graphviz dot to render a PDF file
  //  val projectName = resource.URI.segment(1)
  //	val project = ResourcesPlugin.workspace.root.getProject(projectName)
  //	var path = new File(project.location + "/src-gen/")
  //	var cmd = #["dot", "-Tpdf", fname + ".dot", "-o", fname + ".pdf"]
  //	Runtime.runtime.exec(cmd, null, path).wait

}
