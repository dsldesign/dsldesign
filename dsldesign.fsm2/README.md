The dsldesign.fsm2 examples show how to model a variant of the FSM language,
where initial is an alternative keyword to state, instead of serving as a
standalone declaration (see Exercises).
