// (c) dsldesign, wasowski, tberger
package dsldesign.dateformatter.scala.internal

import scala.annotation.targetName
import scala.language.implicitConversions

case class Date (day: Int, month: Int, year: Int)

private val shortMonths = Array ("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

private def compose (f1: Formatter, f2: Formatter): Formatter =
  Formatter { d => f1.fmt (d) + f2.fmt (d) }

private def compose (f: Formatter, s: String): Formatter =
  Formatter { d => f.fmt (d) + s }

def str (s: String): Formatter = Formatter { _ => s }

val dd = Formatter { d => "%02d".format (d.day) }
val ddth = Formatter { d => d.month match
                           case 1 => "1st"
                           case 2 => "2nd"
                           case 3 => "3rd"
                           case n if n >=4 => s"${n}th" }

val mm = Formatter { d => "%02d".format (d.month) }
val mmm = Formatter { d => shortMonths (d.month - 1) }
val yy = Formatter { d => "%02d".format (d.year % 100) }
val yyyy = Formatter { d => d.year.toString }
val - = str ("-")
val / = str ("/")

given Conversion[String, Formatter] with
  def apply (s: String): Formatter = str (s)

case class Formatter (fmt: Date => String):

  // Apply the formatter to a date
  def apply (d: Date): String = fmt (d)

  // Compose this formatter with another one
  def apply (that: Formatter): Formatter = compose (this, that)

  // Compose this formatter with a constant literal string formatter
  def apply (s: String): Formatter = compose (this, s)

  // Support suffixing of this formatter with another one
  def dd   = compose (this, dsldesign.dateformatter.scala.internal.dd)
  def ddth = compose (this, dsldesign.dateformatter.scala.internal.ddth)
  def mm   = compose (this, dsldesign.dateformatter.scala.internal.mm)
  def mmm  = compose (this, dsldesign.dateformatter.scala.internal.mmm)
  def yy   = compose (this, dsldesign.dateformatter.scala.internal.yy)
  def yyyy = compose (this, dsldesign.dateformatter.scala.internal.yyyy)

  infix def - : Formatter = compose (this, dsldesign.dateformatter.scala.internal.-)
  infix def / : Formatter = compose (this, dsldesign.dateformatter.scala.internal./)

  // Helpers to make the examples smaller
  def print (d: Date): Unit =
    Console.print (this.fmt (d))

  def println (d: Date): Unit =
    Console.println (this.fmt (d))
