// (c) dsldesign, wasowski, tberger
package dsldesign.dateformatter.scala

import scala.language.postfixOps
import internal.*
import internal.given

@main def main =

  val myDate = Date (31, 5, 2022)

  // A simple test (could be moved to a spec file)
  println ("\nTests\n")

  val iso1 = yyyy
  val iso2 = yyyy -
  val iso3 = yyyy - mm
  val iso4 = yyyy - mm -
  val iso  = yyyy - mm - dd

  { iso } println myDate
  { yyyy } println (myDate)
  { yyyy - } println (myDate)
  { yyyy - mm } println (myDate)
  { yyyy - mm - } println (myDate)

  // Demo for the chapter Exercise
  println ("\nDemo\n")

  { yyyy - mm - dd }                         println myDate
  { mm / dd / yyyy }                         println myDate
  { yyyy mm dd }                             println myDate
  { yyyy (".") mm (".") dd }                 println myDate
  { mmm (" ") yy }                           println myDate
  { mmm (" ") ddth (", ") yyyy }             println myDate
  { / mmm (" ") ddth (", ") yyyy / }         println myDate
  { ("(") mmm (" ") ddth (", ") yyyy (")") } println myDate
