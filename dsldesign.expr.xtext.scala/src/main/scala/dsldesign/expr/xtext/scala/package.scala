// (c) dsldesign, wasowski, tberger
package dsldesign.expr.xtext.scala

import scala.jdk.CollectionConverters.*

import dsldesign.expr.*

def prettyPrint (e: Expression): String =
  e match
    case a: AND => 
      s"(${prettyPrint (a.getLeft)} && ${prettyPrint (a.getRight)})"
    case o: OR => 
      s"(${prettyPrint (o.getLeft)} || ${prettyPrint (o.getRight)})"
    case i: Identifier => i.getName
    case n: NOT => "!" + prettyPrint (n.getExpr)
    case _ => "[unknown expression node]"

def printConfiguration (c: Configuration): String =
  c.getName + ": " + c.getLiteral.asScala.map( l =>
    l.getName + "=" + l.isValue ).mkString(", ")

def createConfiguration (name: String, c: List[(String, Boolean)])
  : Configuration =
  val eFactory = ExprFactory.eINSTANCE
  val result = eFactory.createConfiguration
  result.setName (name)
  for (n,v) <- c do
    val i = eFactory.createIdentifier
    i.setName (n)
    i.setValue (v)
    result.getLiteral.add (i)
  result

def eval (e: Expression, c: Configuration): Boolean = e match
  case a: AND => 
    eval (a.getLeft, c) && eval (a.getRight, c)
  case o: OR => 
    eval (o.getLeft, c) || eval (o.getRight, c)
  case i: Identifier => 
    c.getLiteral.asScala
      .find { _.getName == i.getName } 
      .exists { _.isValue }
  case n: NOT => 
    eval (n.getExpr, c)
  case _ => false
