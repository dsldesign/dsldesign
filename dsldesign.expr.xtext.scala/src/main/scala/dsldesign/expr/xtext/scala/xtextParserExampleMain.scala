// (c) dsldesign, wasowski, tberger
package dsldesign.expr.xtext.scala

import dsldesign.expr.{Expression, ExprFactory, ExprPackage}
import dsldesign.expr.xtext.ExprStandaloneSetup
import dsldesign.xtext.scala.parse

@main def xtextParserExampleMain =
  ExprPackage.eINSTANCE.eClass
  given org.eclipse.xtext.ISetup = ExprStandaloneSetup ()

  val expr = "!(a && b) || !c".parse[Expression].get // crash on parse error
  println (prettyPrint (expr))

  val eFactory = ExprFactory.eINSTANCE
  val c1 = createConfiguration ("configuration 1", 
    ("a", true) ::("b",false) ::("c",true) ::Nil)
  println (printConfiguration (c1))
  println (eval (expr, c1))
