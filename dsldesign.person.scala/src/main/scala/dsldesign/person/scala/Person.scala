// (c) dsldesign, wasowski, tberger
package dsldesign.person.scala

import LazyList.empty

class Person (name: String,
              parent: => LazyList[Person] = empty,
              child: => LazyList[Person] = empty):
  def getName = name
  def getChild = child
  def getParent = parent
