// (c) dsldesign, wasowski, tberger
// There is presently no runner for this example
package dsldesign.fsm.scala.transforms

import dsldesign.spreadsheet
import dsldesign.scala.emf.*

// This example is implemented as a copying-trafo directly in Scala
// (no special tools or frameworks).  It computes a value of a RangeSum,
// assuming that all cells in the range contain simple integers (other values
// are ignored). We also assume that the cell range is contigous/convex (so empty cell
// instances exist and are reachable). If a cell's neigbour in a given direction
// is null then there is no more cells in that direction.

object ComputeRangeSum
  extends CopyingTrafo[spreadsheet.RangeSum, Int]:

  import spreadsheet.*

  def sum (cell: Cell, bottomRight: Cell, total: Int): Int =
    if cell == null || cell.column > bottomRight.column || cell.row > bottomRight.row
    then total // finished exploring
    else
      cell.getValue match
      case value: ConstExp => // add the value to the sum
        sum (cell.down, bottomRight,
          sum (cell.right, bottomRight, total + value.getValue))
      case _ => // ignore the value and proceed
        sum (cell.left, bottomRight, sum (cell.right, bottomRight, total))


  override def run (s: RangeSum): Int =
    sum (s.getTopLeft, s.getBottomRight, 0)
