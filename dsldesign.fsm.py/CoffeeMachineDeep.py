# Copyright 2021 Andrzej Wasowski and Thorsten Berger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Import the definition of the DSL (a library now)
from FsmInternalDeep import *

# Build a model  'm'. We mix four different syntaxes,  so these really
# demonstrates  four  different  internal  DSLs for  FSMs  in  Python:
# dictionary-based, keyword argument-based, fluent, and overriding
# operators style.
with Model("coffeeMachine") as m:
     # Variant I: dictionary-based
     step({
         "source": "initial", 
         "input": "coin", 
         "output": "what drink do you want?",
         "target": "selection" })
     step({
         "source": "initial", 
         "input": "idle",
         "target": "initial" })
     step({
         "source": "initial", 
         "input": "break", 
         "output": "machine is broken", 
         "target": "deadlock" })
 
     # Variant II: keyword arguments-based
     transition(source="selection", input="tea",     output="serving tea",                target="makingTea")
     transition(source="selection", input="coffee",  output="serving coffee",             target="makingCoffee")
     transition(source="selection", input="timeout", output="coin returned; insert coin", target="initial")
     transition(source="selection", input="break",   output="machine is broken!",         target="deadlock")

     # Variant III: fluent
     state("makingCoffee") \
         .input ("done").output ("coffee served. Enjoy!").target ("initial") \
         .input ("break").output ("machine is broken!").target ("deadlock")

     # Variant IV: operator style
     # For this to work we are using operators of decreasing precedence.
     # Unlike the Variant III, we cannot chain multiple transitions in
     # a  single expression,  because operator  precedence breaks  (unless we
     # start using many parentheses).  So we choose to start a new transition
     # in a separate statement.  We are using different operators as well.
     # This could work with chaining fluent style, if we used the same operator
     # for the different elements of a transition.
     state("makingTea") ** "done"  % "tea served. Enjoy!" >> "initial"
     state("makingTea") ** "break" % "machine is broken!" >> "deadlock"

     # Designate the initial state (we use it with all the styles above)
     initial ("initial")

# Simple debugging output (here a transformation or an interpreter could be
# started)
print(str(m))
print(f"Created {len(m.tran)} transitions")
print(f"Created {len(m.states)} states: {m.states}")
