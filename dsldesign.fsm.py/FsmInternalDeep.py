# Copyright 2021 Andrzej Wasowski and Thorsten Berger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# An example deeply embedded internal DSL in Python (FSM).  This can be
# translated to Ecore (if you feel the need) using pyecore, but we leave
# the implementation of the transformation as an exercise.

# We implement four different concrete syntaxes,  so these really
# demonstrates  four  different  internal  DSLs for  FSMs  in  Python:
# dictionary-based, keyword argument-based, fluent, and overriding
# operators style.

from __future__ import annotations
from typing import List, Set

# Python is dynamically  typed and needs no type  annotations.  We are
# using type annotations in this example to help you with reading (and
# because we like  the extra assurance from  types). Still, the entire
# thing will work with types erased, too.
#
# We could  have also used  the types much  more systematically. Right
# now the  states, inputs and outputs  are all just strings  (we could
# have defined them as distinct subtypes,  but we did not want to make
# the examples too similar to Scala.

# First, a simple abstract syntax (meta-model) for FSM in Python

class ModelElement:
    """Shared functionality between meta-model elements"""
    def __repr__(self):
        """Support serialization for print-debugging and tests"""
        return str(self.__class__) + ": " + str(self.__dict__) + "\n"

class Tran(ModelElement):
    def __init__(self: Tran, source: str, input: str, output: str, target: str) -> None:
        # These assertions will catch some syntax errors (at runtime)
        assert source != "" and input != "" and target != ""
        self.source = source
        self.input = input
        self.target = target 
        self.output = output

class Model(ModelElement):
    """A simple Python abstract syntax (a meta-model) for FSM"""
    def __init__(self: Model, name: str) -> None:
        self.name = name
        self.initial: str = ""
        self.tran: List[Tran] = []

    @property
    def states(self: Model) -> Set[str]:
        """A derived property listing all states. Should only be used after the
           Model is constructed."""
        return {t.source for t in self.tran} | {t.target for t in self.tran}

    # The  following does  not belong  to  the meta-model  but to  the
    # internal  DSL.  We  start  with an  implementation  with a
    # context manager (if  we are using contexts this  is useful, but
    # otherwise not  strictly necessary). For a deeply  embedded DSL it
    # mostly allows to  show with syntax when we are  "in" the DSL and
    # when outside.
    __contexts: List[Model] = []

    @classmethod
    def context (cls) -> Model:
       return cls.__contexts[-1]

    def __enter__(self: Model) -> Model:
        self.__class__.__contexts.append (self)
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        if exc_type != None: return False
        self.__class__.__contexts.pop()
        return True

# These top-level functions implement the different variants of our deeply
# embedded DSL

def initial(state_name: str) -> None:
    """designate initial state (used in all variants of the internal DSL)"""
    Model.context().initial = state_name

def step(tran: dict) -> None:
    """create a transition (dictionary-based variant)"""
    transition(**tran)

# We use a different name than 'step' because Python does
# not have function overloading (dispatch by the number of parameters)
def transition(**properties) -> None:
    """create a transition (keyword arguments-based variant)"""
    t = Tran(properties["source"], properties["input"], 
             properties.get("output", ""), properties["target"])
    Model.context().tran.append(t)

def state(name: str) -> TranBuilder:
    """create a state (the fluent variant)"""
    return TranBuilder(name)

# The fluent variant continues here
class TranBuilder(ModelElement):
    """
    The transition builder object implements the fluent interface in Python.
    This allows to build and register transitions starting from a single state.
    In contrast to the Scala example, this one is imperative, it builds up a 
    transition and adds it to the context object representing the model. The
    Scala version was constructing an expression representing the model value.
    The Scala version also enforced the grammar by using different types at
    different stages. We have not done this here, to show that a less controlled
    syntax, with a smaller and simpler implementation is also possible (we could
    have used a design with multiple classes in Python as well).
    """
    def __init__(self: TranBuilder, source: str) -> None:
        self.__source = source
        self.reset()
    def reset(self: TranBuilder) -> TranBuilder:
        self.__input = self.__output = self.__target = ""
        return self
    def input(self: TranBuilder, input: str) -> TranBuilder:
        self.__input = input
        return self
    def output(self: TranBuilder, output: str) -> TranBuilder:
        self.__output = output
        return self
    def target(self: TranBuilder, target: str) -> TranBuilder:
        transition(source = self.__source, input = self.__input, 
                output = self.__output, target = target)
        return self.reset()
    """
    The following defs add overloading for operators to support
    the operator style syntax (the last variant). The operator variant
    is fundamentally a fluent interface with overridden operators.
    """
    def __pow__(self: TranBuilder, input: str) -> TranBuilder:
        return self.input (input) 
    def __mod__(self: TranBuilder, msg: str) -> TranBuilder:
        return self.output(msg)
    def __rshift__(self: TranBuilder, target: str) -> TranBuilder:
        return self.target (target)
