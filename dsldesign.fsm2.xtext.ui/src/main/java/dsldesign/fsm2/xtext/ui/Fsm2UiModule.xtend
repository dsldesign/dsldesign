/*
 * generated by Xtext 2.12.0
 */
package dsldesign.fsm2.xtext.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class Fsm2UiModule extends AbstractFsm2UiModule {
}
