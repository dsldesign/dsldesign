// (c) dsldesign, wasowski, tberger
package dsldesign.classmodel.scala

trait NamedElement:
  def name: String

case class Root (classes: List[Class])

case class Class (
  superClass: Option[Class],
  attributes: List[IntegerAttribute],
  name: String
) extends NamedElement

case class IntegerAttribute (
  name: String
) extends NamedElement
