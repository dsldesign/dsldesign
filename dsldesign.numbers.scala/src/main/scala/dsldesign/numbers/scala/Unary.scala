// (c) dsldesign, wasowski, tberger
package dsldesign.numbers.scala

object Unary:

  case class Number (n: Int):
    def I = Number (n+1)
    def I (i :Number): Number = Number (n+2)
    override def toString = n.toString

  object I extends Number (1)


object Unary2: // a version with glued digits

  case class Number (n: Int):
    def I (i: Number) = Number (n + i.n + 1)
    def II (i: Number) = Number (n + i.n + 2)
    def III (i: Number) = Number (n + i.n + 3)
    def IIIII (i: Number) = Number (n + i.n + 5)

    def I = Number (n+1)
    def II = Number (n+2)
    def III = Number (n+3)
    def IIIII = Number (n+5)

    override def toString = n.toString


  object I extends Number (1)
  object II extends Number (2)
  object III extends Number (3)
  object IIIII extends Number (5)


// This little demo is placed here, to avoid multiplying files. Normally this
// should be moved to tests, or a separate file with the main function.

@main def mainUnary =
  import scala.language.postfixOps

  { import Unary.*

    // this is easier to support
    val l1 = List (I, I.I, I.I.I, I.I.I.I, I.I.I.I.I)
    l1 foreach println

    // this is slightly cooler
    println ()
    val l2 = List (I, I I, I I I, I I I I, I I I I I)
    l2 foreach println
  }

  { import Unary2.*
    println ()
    val l3 = List (II II, II III, III III, IIIII IIIII, I III I, I II I, I)
    l3 foreach println
  }
