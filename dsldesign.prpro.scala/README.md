We presently have no front-end (parser) for prpro.  The examples are
in TestCases.scala. The tree `example2` is reasonable (makes sense
probabilistically), and it is processed automatically by
`PrpoGeneratorsSpec` test suite.

The output is printed on stdout, so you can copy it from the terminal
and paste into a file, say `test.py`, then execute the file `python3
test.py`.  The generator assumes that the file `data.csv` is available
in the directory, and defines the columns for the variable in the
model. It is a nice exercise to extend our type checker to check
whether the CSV file and the model are consistent. An example
`data.csv` file can be found in `dsldesign.prpro.py/data.csv`.

After the execution succeeds, you can open the file `test.py.pdf` to
see the results (a similar output is included in a figure int the book).

Hopefully, we will add a front-end for prpro to the repo one day, then
you will be able to run the generator from command line, or even
execute it directly.
