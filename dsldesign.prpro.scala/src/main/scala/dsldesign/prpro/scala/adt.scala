// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

// NB. Almost pure

import types.*, SimpleTy.*, CompositeTy.*

trait Typeable:
  private var ty: Ty = null
  private[adt] def setTy (ty: Ty): Ty =
    this.ty = ty
    ty
  def getTy: Ty = this.ty

enum Declaration extends NamedElement:
  case Let (name: String, value: Expression)
  case Data (name: String, ty: VectorTy)

sealed abstract trait Expression extends Typeable

enum Distribution extends Expression:
  case Uniform (lo: Expression, hi: Expression)
  case Normal (mu: Expression, sigma: Expression)

final case class BExpr (
  left: Expression,
  operator: Operator,
  right: Expression
) extends Expression

final case class VarRef (name: String) extends NamedElement, Expression
final case class CstI (value: Int) extends Expression
final case class CstF (value: Double) extends Expression

enum Operator:
  case Plus, Minus, Mult, Div

type Model = List[Declaration]
