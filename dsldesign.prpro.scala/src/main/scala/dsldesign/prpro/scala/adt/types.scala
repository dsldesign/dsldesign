// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.types

import adt.errors.*
import SimpleTy.*, CompositeTy.*

sealed abstract trait Ty: 

  def isVector = false
  def isSimple: Boolean = false

  def isSubTypeOf (t: Ty): Boolean = (this, t) match 
    // (SSimple)
    case (t1: SimpleTy, t2: SimpleTy) =>
      t1.superTys.contains (t2)
    // (SVect-1)
    case (VectorTy (l1, t1), VectorTy (l2, t2)) =>
      l1 >= l2 && (t1 isSubTypeOf t2)
    // (SVect-2)
    case (VectorTy (l1, t1), t2: SimpleTy) =>
      t1 isSubTypeOf t2
    // (SDist-1)
    case (DistribTy (t1), DistribTy (t2)) =>
      t1 isSubTypeOf t2
    // (SDist-2)
    case (DistribTy (t1), t2: SimpleTy) =>
      t1 isSubTypeOf t2
    case _ => false

  def isSuperTypeOf (t: Ty): Boolean =
    t isSubTypeOf this

end Ty


enum SimpleTy extends Ty:
  case IntTy, NonNegIntTy, NatTy, FloatTy, NonNegFloatTy, 
    PosFloatTy, ProbTy, PosProbTy

  override def isSimple: Boolean = true

  // Represent the reduced subtyping relation
  // (overload for concrete types)

  protected def superTy: Set[SimpleTy] = this match 
    case IntTy => Set (FloatTy)
    case NonNegIntTy => Set (IntTy, NonNegFloatTy)
    case NatTy => Set (NonNegIntTy, PosFloatTy)
    case NonNegFloatTy => Set (FloatTy)
    case PosFloatTy => Set (NonNegFloatTy)
    case ProbTy => Set (NonNegFloatTy)
    case PosProbTy => Set (PosFloatTy, ProbTy)
    case _ => Set()

  // Reflexive transitive closure of the above

  private def _superTys (ts: Set[SimpleTy]): Set[SimpleTy] =
    val result: Set[SimpleTy] = ts ++ (ts.flatMap (_.superTy))
    if result == ts then
      result
    else
      _superTys (result)

  lazy val superTys = _superTys (Set (this))

end SimpleTy



enum CompositeTy extends Ty:
  case VectorTy (len: Int, elemTy: SimpleTy) 
  case DistribTy (outcomeTy: SimpleTy) 

  override def isVector = this match
    case VectorTy (_, _) => true
    case _ => false


private[adt] val topologicallySortedSimpleTys =
  List (NatTy, PosProbTy, NonNegIntTy, PosFloatTy, ProbTy, IntTy,
    NonNegFloatTy, FloatTy)

def lub (t1: SimpleTy, t2: SimpleTy): SimpleTy =
  topologicallySortedSimpleTys
    .find { t => (t isSuperTypeOf t1) && (t isSuperTypeOf t2) }
    .getOrElse (null) // find always succeeds (an offensive null)

def lub (t1: Ty, t2: Ty): Ty = (t1, t2) match
  case (ty1: SimpleTy, ty2: SimpleTy) =>
    lub (ty1, ty2)

  case (VectorTy (len1, ty1), VectorTy (len2, ty2)) =>
    VectorTy (len1 min len2, lub (ty1, ty2))

  case (DistribTy (ty1), DistribTy (ty2)) =>
    DistribTy (lub (ty1, ty2))

  case (VectorTy (len1, ty1), ty2) =>
    lub (ty1, ty2)

  case (DistribTy (ty1), ty2) =>
    lub (ty1, ty2)

  case (ty1: SimpleTy, DistribTy (ty2)) =>
    lub (ty1, ty2)

  case (ty1: SimpleTy, VectorTy (len2, ty2)) =>
    lub (ty1, ty2)
