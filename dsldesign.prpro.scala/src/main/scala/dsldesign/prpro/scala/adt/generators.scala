// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.generators

import org.scalacheck.Gen
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary

import adt.*, Declaration.*, Distribution.*, Operator.*
import types.*, SimpleTy.*, CompositeTy.*

// Generators of types

given Arbitrary[Ty] =
  Arbitrary (Gen.oneOf (arbitrary[SimpleTy], arbitrary[CompositeTy]))

given Arbitrary[SimpleTy] =
  Arbitrary (Gen.oneOf (topologicallySortedSimpleTys))

given Arbitrary[CompositeTy] = Arbitrary {
  Gen.lzy (Gen.oneOf (arbitrary[VectorTy], arbitrary[DistribTy])) }

given Arbitrary[VectorTy] = Arbitrary {
  arbitrary[(Int, SimpleTy)] map { (len, elemTy) => 
    VectorTy (Math.abs (len % 1000) + 1, elemTy) } 
}

given Arbitrary[DistribTy] = 
  Arbitrary (arbitrary[SimpleTy] map DistribTy.apply)

// Generators of models (of abstract syntax)

given Arbitrary[Expression] = Arbitrary {
  Gen.lzy {
    Gen.frequency (
      1 -> arbitrary[Distribution],
      1 -> arbitrary[BExpr],
      2 -> arbitrary[VarRef],
      2 -> arbitrary[CstI],
      2 -> arbitrary[CstF]
    )
  }
}

given Arbitrary[Declaration] =
  Arbitrary (Gen.oneOf (arbitrary[Data], arbitrary[Let]))

given Arbitrary[Distribution] = 
  Arbitrary (Gen.oneOf (arbitrary[Uniform], arbitrary[Normal]))

given Arbitrary[Let] = 
  Arbitrary (arbitrary[(String, Expression)]  map Let.apply)

given Arbitrary[Data] =
  Arbitrary (arbitrary[(String, VectorTy)] map Data.apply)

given Arbitrary[Uniform] =
  Arbitrary (arbitrary[(Expression, Expression)] map Uniform.apply)

given Arbitrary[Normal] =
  Arbitrary (arbitrary[(Expression, Expression)] map  Normal.apply)

given Arbitrary[BExpr] =
  Arbitrary (arbitrary[(Expression,Operator,Expression)] map BExpr.apply)

given Arbitrary[VarRef] = Arbitrary (arbitrary[String] map VarRef.apply)

given Arbitrary[CstI] = Arbitrary (arbitrary[Int] map CstI.apply)

given Arbitrary[CstF] = Arbitrary (arbitrary[Double] map CstF.apply)

given Arbitrary[Operator] = 
  Arbitrary (Gen.oneOf (Plus, Mult, Div, Minus))

given Arbitrary[Model] = Arbitrary {
  Gen.chooseNum (0, 500)
    .flatMap { len => Gen.listOfN (len, arbitrary[Declaration]) }
}
