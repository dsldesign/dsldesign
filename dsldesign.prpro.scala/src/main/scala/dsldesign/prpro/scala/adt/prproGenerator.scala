// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.prproGenerator

import org.typelevel.paiges.Doc

import adt.*, Declaration.*, Distribution.*, Operator.*
import types.*

import adt.typeChecker.{DataEnv, TypeEnv}

private def generate (ope: Operator): Doc = ope match
  case Plus => Doc.str ("+")
  case Minus => Doc.str ("-")
  case Mult => Doc.str ("*")
  case Div => Doc.str ("/")

private val QUOTE: Doc = Doc.char ('"')
private def quote (d: Doc): Doc = QUOTE + d + QUOTE
private def quote (s: String): Doc = quote (Doc.text (s))
private def paren (d: Doc): Doc = Doc.str("(") + d + Doc.str(")")
private def paren (s: String): Doc = paren (Doc.text (s))

private def varName (context: Option[Declaration]): Doc =
  context
   .map { decl => quote (Doc.str (decl.name)) + Doc.comma + Doc.space }
   .getOrElse (Doc.empty)

private def observed (denv: DataEnv, context: Option[Declaration]): Doc =
  context
    .filter { decl => denv.isDefinedAt (decl.name) }
    .map { decl => generate (denv, None, VarRef (decl.name))  }
    .map { o => Doc.str (", observed = ") + o }
    .getOrElse (Doc.empty)

def generate (denv: DataEnv, context: Option[Declaration], e: Expression): Doc =
  e match 
  case CstI (n) =>
    Doc.str (n)

  case CstF (x) =>
    Doc.str (x)

  case VarRef (name) =>
    if denv isDefinedAt name then
       Doc.str ("data[") + quote (name) + Doc.str ("]")
    else
       Doc.str ("prpro_") + Doc.str (name)

  case BExpr (left, operator, right) =>
    val dLeft = generate (denv, None, left)
    val dRight = generate (denv, None, right)
    val dOpe = generate (operator)
    paren (dLeft + dOpe + dRight)

  case Normal (mu, sigma) =>
    val dMu = Doc.str ("mu = ") + generate (denv, None, mu)
    val dSigma = Doc.str (", sigma = ") + generate (denv, None, sigma)
    val dObserved = observed (denv, context)
    paren (Doc.str ("pm.Normal") + paren (
      varName (context) + dMu + dSigma + dObserved))

  case Uniform (lo, hi) =>
    val dLo = generate (denv, None, lo)
    val dHi = generate (denv, None, hi)
    val dObserved = observed (denv, context)
    paren (Doc.str ("pm.Uniform") + paren (
      varName (context) + dLo + Doc.comma + Doc.space + dHi + dObserved))
  

def generate (denv: DataEnv, l: Let): Doc =
  val rhs = generate (denv, Some (l), l.value)
  Doc.str ("prpro_") + Doc.str (l.name) + Doc.str (" = ") + rhs

def body (denv: DataEnv, m: Model): Doc =
  val lets = m collect { case l: Let => l }
  val docs = lets map { l => generate (denv, l) }
  Doc.intercalate (Doc.lineBreak, docs)

def plots (denv: DataEnv, tenv: TypeEnv, m: Model): Doc =
  val parameters = tenv.keySet.diff (denv.keySet)
  parameters
    .toList
    .zipWithIndex
    .map { (name, i) =>
      s"az.plot_posterior(trace, var_names = ['${name}'], ax = ax[1,${i}])" }
    .map { Doc.str _ }
    .foldRight[Doc] (Doc.empty) (_ / _)

def generate (denv: DataEnv, tenv: TypeEnv, m: Model): String =
  // We assume that there are only two variables: a regressor (first) and
  // the target (second). See exercises for lifting this limitation.
  val x = denv.keys.toList (0)
  val y = denv.keys.toList (1)
  val N = tenv.keySet.diff (denv.keySet).size
  s"""
     |import arviz as az
     |import matplotlib.pyplot as plt
     |import pandas as pd
     |import pymc3 as pm

     |print('loading data.csv')
     |data = pd.read_csv('./data.csv')
     |print(data.head())
     |print('...')

     |with pm.Model() as model:
     |    ${body(denv, m).hang (4).render (-1)}
     |    trace = pm.sample(draws = 20000, return_inferencedata = True)

     |print('creating plots')

     |N = ${N} # numbers of parameters (non-data backed variables) in the model

     |fig, ax = plt.subplots(2, N, figsize=(15, 10))
     |ax[0,0].plot(data['${x}'], data['${y}'], 'o', label='data', alpha = 0.3)
     |ax[0,0].set_xlabel('${x}')
     |ax[0,0].set_ylabel('${y}')
     |ax[0,0].set_title('A scatter plot of the data')

     |${plots (denv, tenv, m).render (-1)}
     |fig.tight_layout()

     |print(f'generating a pdf file {__file__}.pdf')
     |plt.savefig(f'{__file__}.pdf')
  """.stripMargin
