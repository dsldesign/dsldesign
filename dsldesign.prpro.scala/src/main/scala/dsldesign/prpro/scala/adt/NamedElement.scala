// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

abstract trait NamedElement:
  val name: String
