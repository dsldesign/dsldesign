// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.errors

type ErrMessage = String
type Result[+T] = Either[ErrMessage, T] 
