// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.typeChecker

import adt.*, Declaration.*, Distribution.*, Distribution.*
import types.*, SimpleTy.*, CompositeTy.*
import adt.errors.*

type TypeEnv = Map[String, Ty]
type DataEnv = Map[String, VectorTy]

val emptyTypeEnv: TypeEnv = Map ()
val emptyDataEnv: DataEnv = Map ()

extension [T] (res: Result[T])
  def failIf (p: T => Boolean, msg: T => String): Result[T] =
    res.flatMap { t => if p (t) then Left (msg (t)) else res }

  def ensure (p: T => Boolean, msg: T => String): Result[T] =
    res.flatMap { t => if p(t) then res else Left (msg (t)) }


def tyCheck (tenv: TypeEnv, denv: DataEnv, model: Model)
  : Result[(TypeEnv, DataEnv)] =
  model.foldLeft (Right (tenv,denv)) { tyCheck _ }

def tyCheck (res: Result[(TypeEnv, DataEnv)], decl: Declaration)
  : Result[(TypeEnv, DataEnv)] =
  res.flatMap { (tenv, denv) => tyCheck (tenv, denv, decl) }

def tyCheck (tenv: TypeEnv, denv: DataEnv, decl: Declaration)
  : Result[(TypeEnv, DataEnv)] = decl match
  case Data (name, ty) =>
    val denv1 = denv.get (name) match
      case Some (_) => Left (s"Data for '$name' has already been registered!")
      case None     => Right (denv + (name -> ty))
    denv1.map { denv1 => (tenv, denv1) }

  case Let (name, value) =>
    val tenv1 = tyCheck (tenv, denv, value)
      .ensure (
        t1 => tenv.get (name).isEmpty,
        t1 => s"'$name' has already been defined!" )
      .map { t1 => tenv + (name -> t1) }
    tenv1.map { tenv1 => (tenv1, denv) }


def tyCheck (tenv: TypeEnv, denv: TypeEnv, expr: Expression): Result[Ty] =
  expr match
    case BExpr (left, operator, right) =>
      for
        t1 <- tyCheck (tenv, denv, left)
        t2 <- tyCheck (tenv, denv, right)
      yield expr.setTy (lub (t1, t2)) // Unsound for minus and other operators
                                      // See exercises to fix this
    case CstI (n) if n > 0 =>
      Right (expr.setTy (NatTy))

    case CstI (0) =>
      Right (expr.setTy (NonNegIntTy))

    case CstI (_) =>
      Right (expr.setTy (IntTy))

    case CstF (0.0) =>
      Right (expr.setTy (ProbTy))

    case CstF (x) if x > 0.0 && x <= 1.0 =>
      Right (expr.setTy (PosProbTy))

    case CstF (x) if x > 1.0 =>
      Right (expr.setTy (PosFloatTy))

    case CstF (_) =>
      Right (expr.setTy (FloatTy))

    case VarRef (name) =>
      denv
        .get (name)
        .orElse (tenv.get (name))
        .toRight (s"Undeclared variable '${name}'")
        .map (expr.setTy)

    case Normal (mu, sigma) =>
      for
        _ <- tyCheck (tenv, denv, mu).ensure (
            t => t.isSubTypeOf (FloatTy),
            t => s"Need a sub-type FloatTy for 'mu' but got '$t'" )
        _ <- tyCheck (tenv, denv, sigma).ensure (
            t => t.isSubTypeOf (NonNegFloatTy),
            t => s"Need a sub-type of NonNegFloatTy for 'sigma' but got '$t'" )
      yield expr.setTy (DistribTy (FloatTy))

    case Uniform (lo, hi) =>
      for
        t0 <- tyCheck (tenv, denv, lo)
        t1 <- tyCheck (tenv, denv, hi)
        t  <- lub (t0, t1) match
          case t: SimpleTy => Right (t)
          case DistribTy (t) => Right (t)
          case t =>
            Left (s"Endpoint types must be sub-types of simple type (got '$t')")
      yield expr.setTy (DistribTy (t))

  end match 
