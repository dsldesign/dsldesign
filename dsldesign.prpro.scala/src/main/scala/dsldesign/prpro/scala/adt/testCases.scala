package dsldesign.prpro.scala
package adt.testCases

import adt.*, Declaration.*, Distribution.*, Operator.*
import types.*, SimpleTy.*, CompositeTy.*

val example1: Model = List (
  Data ("x", VectorTy(17, PosFloatTy)),
  Data ("y", VectorTy(17, FloatTy)),

  Let ("Beta0", Uniform (CstI (-200), CstI(200))),
  Let ("Beta1", Uniform (CstI (-200), CstI(200))),
  Let ("sigma", Uniform (CstF (0), CstI (100)) ),

  Let ("y",
    Normal(
      mu = BExpr (
        BExpr (VarRef ("Beta1"), Mult, VarRef ("x")),
        Plus,
        VarRef ("Beta0")),
     sigma = VarRef ("sigma") )
  )
)

val example2: Model = List (

  Data ("x", VectorTy(500, PosFloatTy)),
  Data ("y", VectorTy(500, FloatTy)),

  Let ("b0", Uniform (CstI (-2), CstI(2))),

  Let ("b1", Uniform (CstI (-2), CstI(2))),

  Let ("sigma",
    Uniform (CstF (0.001), CstF  (2.0)) ),

  Let ("y",
    Normal(
      mu = BExpr (
        BExpr (VarRef ("b1"), Mult, VarRef ("x")),
        Plus,
        VarRef ("b0")),
     sigma = VarRef ("sigma") )
  )
)
