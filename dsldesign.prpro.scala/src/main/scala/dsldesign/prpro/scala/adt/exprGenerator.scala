// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt.exprGenerator

import adt.*, Distribution.*, Operator.*
import types.*, SimpleTy.*, CompositeTy.*
import StringTree.*

// A simple tree-based generation structure (could be moved elsewhere)

enum StringTree:
  case Em
  case Lf (s: String)
  case Br (pr: StringTree, sf: StringTree)

  infix def |+| (sf: StringTree): StringTree = Br (this, sf)
  infix def |+| (sf: String): StringTree = Br (this, Lf (sf))

  override def toString: String = this.toStringList(Nil).mkString

  def toStringList (l: List[String]): List[String] = this match
    case Em => l
    case Lf (s) => s::l
    case Br (pr, sf) => 
      pr.toStringList (sf.toStringList (l))


extension (pr: String)
  def |+| (sf: StringTree): StringTree = Br (Lf (pr), sf)
  def |+| (sf: String): StringTree = Br (Lf (pr), Lf (sf))

 
// A generator for expressions using the above trees as output

type DataEnv = Map[String, VectorTy]

private def generate (ope: Operator): String = ope match
  case Plus => "+"
  case Minus => "-"
  case Mult => "*"
  case Div => "/"

private def varName (context: Option[Declaration]): StringTree =
  Lf {
    context
     .map { decl => s""""${decl.name}", """ }
     .getOrElse ("")
  }

private def observed (denv: DataEnv, context: Option[Declaration])
  : Option[StringTree] =
  for 
    decl <- context
    o = generate(denv, None, VarRef (decl.name)) 
        if denv.isDefinedAt (decl.name)
  yield Lf (s", observed = ${o}")

def generate (denv: DataEnv, context: Option[Declaration], e: Expression)
  : StringTree = e match
  case CstI (n) =>
    Lf (n.toString)

  case CstF (x) =>
    Lf (x.toString)

  case VarRef (name) =>
    if denv isDefinedAt name then
      "data['" |+| name |+| "']"
    else
      "prpro_" |+| name

  case BExpr (left, operator, right) =>
    val sLeft = generate (denv, None, left)
    val sRight = generate (denv, None, right)
    val sOpe = generate (operator)
    "(" |+| sLeft |+| sOpe |+| sRight |+| ")"

  case Normal (mu, sigma) =>
    val sMu = "mu = " |+| generate (denv, None, mu)
    val sSigma = ", sigma = " |+| generate (denv, None, sigma)
    val sObserved = observed (denv, context).getOrElse (Em)
    "(Normal(" |+| varName (context) |+| sMu |+| sSigma |+| sObserved |+| "))"

  case Uniform (lo, hi) =>
    val sLo = generate (denv, None, lo)
    val sHi = generate (denv, None, hi)
    val sObserved = observed (denv, context).getOrElse (Em)
    "(Uniform(" |+| varName (context) |+| sLo |+| ", " |+| sHi |+| sObserved |+| "))"
