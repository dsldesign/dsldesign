// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

import Declaration.*, Distribution.*, Operator.*
import types.*, SimpleTy.*, CompositeTy.*

class PureSpec 
  extends org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.must.Matchers,
  org.scalatest.prop.Configuration,
  org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  "Example AST" -  {

    "example 1 shall compile" in {

      """
        val m: Model = List (

          Data ( "x", VectorTy (500, PosFloatTy)),
          Data ( "y", VectorTy (500, PosFloatTy)),

          Let ( "β0", Uniform (CstI (-200), CstI(200))),

          Let ( "β1", Uniform (CstI (-200), CstI(200))),

          Let ( "σ", 
            Uniform (CstI (0), CstI  (100)) ),

          Let ( "y", 
            Normal( mu = BExpr (BExpr ( VarRef ("β1"), Mult, VarRef ("x") ), Plus, VarRef ("β0")), 
            VarRef ("σ") )
          )
        )
      """ must compile

    }

  }
