// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

import typeChecker.*, Declaration.*
import types.*

class ExprGeneratorSpec
  extends org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.must.Matchers,
  org.scalatest.prop.Configuration,
  org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  val tenv0 = emptyTypeEnv
  val denv0 = emptyDataEnv

  "Test cases " -  {

    // TODO: one way to test is two use two generators against each other
    "00 Example 1 generation (just prints, only no-crash test)" in {

        val result = tyCheck (tenv0, denv0, testCases.example1)
        result must be (Symbol ("Right"))

        // getOrElse will always succeed, the argument is immaterial
        val (tenv,denv) = result.getOrElse (null)

        for 
          d <- testCases.example1
        do 
          d match
          case Let (name, right) =>
             val st = exprGenerator.generate (denv, Some (d), right)
             info (s"The declaration of '${name}'")
             info (st.toString())
             info ("-------------------------------------------")
          case _ => ()
    }

  }
