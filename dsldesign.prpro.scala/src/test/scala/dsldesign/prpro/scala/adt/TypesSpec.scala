// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

import types.*, SimpleTy.*, CompositeTy.*
import adt.generators.given

class TypesSpec
  extends org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.must.Matchers,
  org.scalatest.prop.Configuration,
  org.scalatest.Inside,
  org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  "Case-based regressions" -  {

    "isSubTypeOf (vector length is contravariant)" in {

      val result = VectorTy (1, FloatTy)
        .isSubTypeOf (VectorTy (2, FloatTy))
      result must
        be (false)
    }

    "lub (ProbTy, NonNegFloatTy)" in {

      lub (ProbTy, NonNegFloatTy) must
        be (NonNegFloatTy)
    }

    "Is it least? (a case-based test, several non-trivial cases)" in {

      lub (NatTy, PosProbTy) must
        be (PosFloatTy)
      lub (PosProbTy, IntTy) must
        be (FloatTy)
      lub (ProbTy, IntTy) must
        be (FloatTy)
    }

    "DistribTy(PosProbTy) isSubTypeOf NonNegFloatTy" in {
      (DistribTy (PosProbTy) isSubTypeOf NonNegFloatTy) must
        be (true)
    }

  }

  "The partial order of SimpleTy" - {

    "sub-typing is reflexive" in
      forAll ("t") {
        (t: SimpleTy) =>
          (t isSubTypeOf t) must
            be (true)
      }

    "sub-typing is anti-symmetric for simple types" in
      forAll ("t1", "t2", maxDiscardedFactor (100)) {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t1)) {
            t1 must
              be (t2)
          }
      }

    "sub-typing is transitive for simple types" in
      forAll ("t1", "t2", "t3", maxDiscardedFactor (100)) {
        (t1: SimpleTy, t2: SimpleTy, t3: SimpleTy) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t3)) {
            t1.isSubTypeOf (t3) must
              be (true)
          }
      }

    "super-typing is reflexive" in
      forAll ("t") {
        (t: SimpleTy) =>
          (t isSuperTypeOf t) must
            be (true)
      }

    "lub is reflexive (idempotent) on simple types" in
      forAll ("ty") {
        (ty: SimpleTy) =>
          lub (ty, ty) must
            be (ty)
      }

    "lub is one of its arguments if one subtype of the other (left)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t1 isSubTypeOf t2) {
            lub (t1, t2) must
              be (t2)
          }
      }

    "lub is one of its arguments if one subtype of the other (right)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t2 isSubTypeOf t1) {
            lub (t1, t2) must
              be (t1)
          }
      }

    "Axiom: lub is a supertype of both of its arguments" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          val t = lub (t1, t2)
          t.isSuperTypeOf (t2) must
            be (true)
          t.isSuperTypeOf (t1) must
            be (true)
    }

    "Axiom: lub is least such" in {

      // It is fairly difficult to randomly generate
      // the triple satisfying the condition, so
      // we tell the generator to try harder

      given PropertyCheckConfiguration (maxDiscardedFactor = 100.0)

      forAll ("t", "t1", "t2") {
        (t: SimpleTy, t1: SimpleTy, t2: SimpleTy) =>
          whenever (t.isSuperTypeOf (t1) && t.isSuperTypeOf (t2)) {
            val ty = lub (t1, t2)
            withClue (s"{lub (t1, t2) = $ty}") {
              (t isSuperTypeOf ty) must
                be (true)
            }
          }
      }
    }

  }

  "The partial order on Ty" - {

    "sub-typing is reflexive" in
      forAll ("t") { (t: Ty) =>
        (t isSubTypeOf t) must
          be (true)
      }

    "sub-typing is anti-symmetric" in
      forAll ("t1", "t2", maxDiscardedFactor (100)) {
        (t1: Ty, t2: Ty) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t1)) {
            t1 must
              be (t2)
          }
      }

    "sub-typing is transitive" in
      forAll ("t1", "t2", "t3", maxDiscardedFactor (100)) {
        (t1: Ty, t2: Ty, t3: Ty) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t3)) {
            t1.isSubTypeOf (t3) must
              be (true)
          }
      }

    "super-typing is reflexive" in
      forAll ("t") { (t: Ty) =>
          (t isSuperTypeOf t) must
            be (true)
      }

    "lub is able to unify vectors of simple types" in
      forAll ("t1","t2", "l1", "l2") {
        (t1: SimpleTy, t2: SimpleTy, n: Int, m: Int) =>
          val l1 = Math.abs (n)
          val l2 = Math.abs (m)
          lub (VectorTy (l1, t1), VectorTy (l2, t2)) must
            be  (VectorTy (Math.min (l1,l2), lub (t1,t2)))
      }

    "lub (DistribTy (t1), DistribTy (t2)) = D (lub (t1, t2))" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
            lub (DistribTy (t1), DistribTy (t2)) must
              be (DistribTy (lub (t1, t2)))
      }

    "lub (DistribTy (t1), t2) = lub (t1, t2)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          lub (DistribTy (t1), t2) must
            be (lub (t1,t2))
          lub (DistribTy (t2), t1) must
            be (lub (t1, t2))
      }

    "lub on Ty delegates to lub on SimpleTy" in
      forAll ("t1","t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          lub (t1: Ty, t2: Ty) must be (lub (t1, t2))
      }

    "Commutative" in
      forAll ("t1","t2") {
        (t1: Ty, t2: Ty) =>
          lub (t1, t2) must be (lub (t2, t1))
      }

    "'fuzzing'" in
      forAll ("t1", "t2") {
        (t1: Ty, t2: Ty) =>
          noException must be thrownBy lub (t1, t2)
      }

    "Axiom: lub is a supertype of both of its arguments if it exists" in {
      forAll ("t1", "t2") {
        (t1: Ty, t2: Ty) =>
          val ty = lub (t1, t2)
          withClue (s"{$ty isSuperTypeOf $t1}") {
            (ty isSuperTypeOf t1) must
            be (true)
          }
          withClue (s"{$ty isSuperTypeOf $t2}") {
            (ty isSuperTypeOf t2) must
            be (true)
          }
      }
    }

    // The same test as above, slightly less precise, but also more
    // concise for the book presentation

    "Axiom: lub is a supertype of its arguments" in {
       forAll { (t1: Ty, t2: Ty) =>
          t1.isSubTypeOf (lub (t1,t2)) must be (true)
          t2.isSubTypeOf (lub (t1,t2)) must be (true)
       }
    }

    "Axiom: lub is least such" in {
      // It is fairly difficult to randomly generate
      // the triple satisfying the condition, so
      // we tell the generator to try harder

      forAll ("t", "t1", "t2", maxDiscardedFactor (800)) {
        (t: Ty, t1: Ty, t2: Ty) =>
          whenever (t.isSuperTypeOf (t1) && t.isSuperTypeOf (t2)) {
             t.isSuperTypeOf (lub (t1, t2)) must be (true)
          }
      }
    }

    // The same test as above, slightly less precise, but also more
    // concise for the book presentation

    "join is least such" in
      forAll (maxDiscardedFactor (100)) { (t: Ty, t1: Ty, t2: Ty) =>
        whenever (t1.isSubTypeOf (t) && t2.isSubTypeOf (t)) {
            lub (t1, t2).isSubTypeOf (t) must be (true)
          }
        }

  }
