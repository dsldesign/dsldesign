// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

import adt.generators.given
import Declaration.*, Distribution.*
import typeChecker.*, types.*, SimpleTy.*, CompositeTy.*

class TypeCheckerSpec
  extends org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.must.Matchers,
  org.scalatest.prop.Configuration,
  org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  val tenv0 = emptyTypeEnv
  val denv0 = emptyDataEnv

  "Test cases " -  {

    "00 Example 1 shall typeCheck" in {
        tyCheck (tenv0, denv0, testCases.example1) must be (Symbol ("Right"))
    }

    "01 Example 2 shall typeCheck" in {
        tyCheck (tenv0, denv0, testCases.example2) must be (Symbol ("Right"))
    }

    "02 Regression for Uniform (dist, dist)" in {
      val e = Uniform (
        lo = Normal (CstF (0.0), CstF (0.1)),
        hi = Normal (CstF (42.0), CstF (0.1)))
      tyCheck (tenv0, denv0, e) must be (Right (DistribTy (FloatTy)))
    }

    "03 Regression name conclicts (Data; Data)" in {
        val m: Model = List (
          Data ("x", VectorTy(23, PosFloatTy)),
          Data ("x", VectorTy(23, PosFloatTy)))
        tyCheck (tenv0, denv0, m) must be (Symbol ("Left"))
    }

    "04 Regression name conclicts (Let; Let); Data)" in {
        val m: Model = List (
          Let ("x", Uniform (CstI (-200), CstI(200))),
          Let ("x", Uniform (CstI (-100), CstI(100))))
        tyCheck (tenv0, denv0, m) must be (Symbol ("Left"))
    }

  }

  "Typechecker 'fuzzing'" - {

    "A 'fuzz' test for CstI" in {
      forAll ("cst") {
         (cst: CstI) =>
           noException must be thrownBy tyCheck (tenv0, denv0, cst)
      }
    }

    "A 'fuzz' test for VarRef" in {
      forAll ("v") {
         (v: VarRef) =>
           noException must be thrownBy tyCheck (tenv0, denv0, v)
      }
    }

    "A 'fuzz' test for Uniform" in {
      forAll ("d") {
         (d: Uniform) =>
           noException must be thrownBy tyCheck (tenv0, denv0, d)
      }
    }

    "A 'fuzz' test for Normal" in {
      forAll ("d") {
         (d: Normal) =>
           noException must be thrownBy tyCheck (tenv0, denv0, d)
      }
    }

    "A 'fuzz' test for BExpr" in {
      forAll ("expr") {
         (expr: BExpr) =>
           noException must be thrownBy tyCheck (tenv0, denv0, expr)
      }
    }

    "A 'fuzz' test for Expression" in {
      forAll ("expr") {
         (expr: Expression) =>
           noException must be thrownBy tyCheck (tenv0, denv0, expr)
      }
    }

    "A 'fuzz' test for Model" in {
      forAll ("model") {
         (model: Model) =>
           noException must be thrownBy tyCheck (tenv0, denv0, model)
      }
    }

  }
