// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala
package adt

import adt.typeChecker.*

class PrproGeneratorSpec
  extends org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.must.Matchers,
  org.scalatest.prop.Configuration,
  org.scalatestplus.scalacheck.ScalaCheckPropertyChecks:

  val tenv0 = emptyTypeEnv
  val denv0 = emptyDataEnv

  "Test cases " -  {

    "00 Example 2 generation (just prints, only no-crash test)" in {

        val result = tyCheck (tenv0, denv0, testCases.example2)
        result must be (Symbol ("Right"))

        // getOrElse will always succeed, the argument is immaterial
        val (tenv,denv) = result.getOrElse (null)

        info ("-------------------------------------------")
        info (prproGenerator.generate (denv, tenv, testCases.example2))
        info ("-------------------------------------------")
    }

  }
