// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.xtext.scala

import scala.jdk.CollectionConverters.*

import dsldesign.fsm.Model
import dsldesign.fsm.xtext.FsmStandaloneSetup
import dsldesign.xtext.scala.*

import org.scalatest.matchers.should.Matchers
import org.scalatest.freespec.AnyFreeSpec

class ParsersSpec extends AnyFreeSpec, Matchers:

  class Fixture:
    private lazy val setup = FsmStandaloneSetup ()
    given FsmStandaloneSetup = setup

  "Precise grammar element tests" - {

    "Empty model" in new Fixture:
      val result = "".parse[Model]
      result should not be empty
      for m <- result 
      yield m.getMachines shouldBe empty

    "One machine, no states" in new Fixture:
      val result = "machine Square".parse[Model]
      result should not be empty
      for m <- result 
      yield
        m.getMachines.size shouldBe 1
        m.getMachines.asScala.head.getName shouldBe "Square"

    "One machine, no states []" in new Fixture:
      val result = """machine "Square" []""".parse[Model]
      result should not be empty
      for m <- result 
      yield 
        m.getMachines.size shouldBe 1
        m.getMachines.asScala.head.getName shouldBe "Square"

    "A correct initial state reference" in new Fixture:
      val result = """
          machine MACHINE [
            initial STATE
            state STATE
          ]
        """.parse[Model]
      result should not be None
      for mo <- result
      yield
        mo.getMachines.asScala.size shouldBe 1
        val ma = mo.getMachines.asScala.head
        ma.getStates.asScala.size shouldBe 1
        val st = ma.getStates.asScala.head
        st.getName shouldBe "STATE"
        ma.getInitial shouldBe st

    "All orderings of initial" in new Fixture:
      """machine MACHINE [
           initial STATE
           state STATE
         ]
      """.parse[Model] should not be None

      """machine MACHINE [
           state STATE []
           initial STATE
         ]
      """.parse[Model] should not be None

      """machine MACHINE [
           state STATE1
           initial STATE
           state STATE []
         ]
      """.parse[Model] should not be None

    "Transition variations" in new Fixture:
      val result = """
        machine MACHINE [
          initial STATE
          state STATE [
            on input INPUT output OUTPUT and go to STATE
            on INPUT go to STATE
          ]
        ]
      """.parse[Model]
      result should not be None
  }

  "Errors with syntax errors" - {

    "An unknown unexpected token" in new Fixture:
      "alamakota".parse[Model] shouldBe None

    // Apparently Xtext does not report linking errors at loading?
    // Or there is a bug in the following test
    "A dangling initial state reference" ignore new Fixture:
      "machine MACHINE [ initial STATE ]".parse[Model] shouldBe None

    "A machine without initial state" in new Fixture:
      "machine MACHINE [ state STATE [] ]".parse[Model] shouldBe None
  }
