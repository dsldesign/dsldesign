// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.xtext.scala

import scala.util.Try

class GeneratorsSpec extends
  org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.should.Matchers:

  "Integration test" - {

    "Xtext parsers and generators to Java and Dot run without throwing exceptions" - {
      "CoffeeMachine.fsm" in {
        Try (Generators ("../dsldesign.fsm/test-files/CoffeeMachine.fsm"))
          .shouldBe (Symbol ("success"))
      }

      "Complete.fsm" in {
        Try (Generators ("../dsldesign.fsm/test-files/Complete.fsm"))
          .shouldBe (Symbol ("success"))
      }
    }
  }
