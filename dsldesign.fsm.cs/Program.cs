﻿// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Linq;
using DslDesign.Fsm;

using NMF.Models.Repository;
using NMF.Models;

[assembly: ModelMetadata("http://www.dsl.design/dsldesign.fsm", "Constraints.fsm.nmf")]

class Constraints
{
  static Func<IFiniteStateMachine,bool> C2 = 
    m => ( from s1 in m.States 
           from s2 in m.States 
           where s1 != s2
           select s1.Name == s2.Name
         ).All (x => !x);


  // If you find the relational style quirky, or if you dislike how the
  // use of .All mismatches the syntactic style of LINQ above, we bring
  // the following non-LINQ version for you:
  static Func<IFiniteStateMachine, bool> C2a =  m =>
    m.States.All ( s1 =>
        m.States.All ( s2 => s1==s2 || s1.Name!=s2.Name ) );


  private static ModelRepository repository = new ModelRepository();

  // Load a test case
  static DslDesign.Fsm.Model LoadInstance (string fname) 
  {
    Console.WriteLine ($"Loading '{fname}'...");
    return repository
      .Resolve (fname)
      .RootElements[0] as DslDesign.Fsm.Model;
  }


  // Check constraint C2 on all machines in the file designated by fname
  static void CheckAll (string fname) {
    var results = 
      LoadInstance ($"../dsldesign.fsm/test-files/{fname}.xmi")
      .Machines
      .Select ( m => Tuple.Create(m, C2 (m)) );

    if (results.All (r => r.Item2))
      Console.WriteLine ($"C2 is satisfied by all machines in {fname}.xmi");
    else 
      foreach (var name in 
          results.Where (r => !r.Item2).Select (r => r.Item1.Name))
        Console.WriteLine ($"C2 VIOLATED by machine '{name}' in {fname}.xmi");
  }



  static void Main(string[] args)
  {
    var testModels = new string[] { "test-00", "test-01", "test-02",
      "test-07", "test-09", "test-11", "test-04", "test-14", "test-08" };

    Console.WriteLine ("C# example in dsldesign.fsm.cs/");
    foreach (var t in testModels) CheckAll (t);
  }
}
