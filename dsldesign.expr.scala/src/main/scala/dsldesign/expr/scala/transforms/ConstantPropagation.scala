// (c) dsldesign, berger, wasowski
// A constant propagation (more properly a constant substition) trafo
package dsldesign.expr.scala.transforms

import org.bitbucket.inkytonik.kiama.rewriting.Cloner.{everywherebu, rule}
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

import dsldesign.expr.scala.adt.*
import dsldesign.scala.emf.*

class ConstantPropagation (val p: Configuration) extends
  CopyingParameterizedTrafo[Expression, Configuration, Expression]:

  val constantPropagationRule = everywherebu {
    rule[Expression] {
      case id@Identifier (_)
        if p.identifierValues.keySet.contains (id) =>
          p.identifierValues.get (id).get
    }
  }

  override def run (self: Expression): Expression =
    Rewriter.rewrite (constantPropagationRule) (self)


// A main function to run the trafo

import scala.language.implicitConversions
import dsldesign.expr.scala.adt.StringToExpression

@main def constantPropagationMain =
  val self = "a" & "b" & "c"
  println (self)
  val configuration = Configuration ("test", Map (Identifier("b") -> True))
  println (configuration)

  val partiallyConfigured = ConstantPropagation (configuration).run (self)
  println (partiallyConfigured)
