// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.transforms

import org.bitbucket.inkytonik.kiama.rewriting.Cloner.{everywheretd, innermost, reduce, rule}
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

import dsldesign.expr.scala.adt.*
import dsldesign.scala.emf.*

object ExpressionToCNF extends CopyingTrafo[Expression, Expression]:

  val demorgansRule = reduce {
    rule[Expression] {
      case NOT (AND (x, y)) => OR (NOT (x), NOT (y))
      case NOT (OR (x, y)) => AND (NOT (x), NOT (y))
    }
  }

  val doubleNegationRule = reduce {
    rule[Expression] {
      case NOT (NOT (x)) => x
    }
  }

  val valueNegationRule = everywheretd {
    rule[Expression] {
      case NOT (True) => False
      case NOT (False) => True
    }
  }

  val distributiveRule = innermost {
    rule[Expression] {
      case OR (x, AND (y, z)) => AND (OR (x, y), OR (x, z))
      case OR (AND (x, y), z) => AND (OR (x, z), OR (y, z))
    }
  }

  def run (self: Expression): Expression =
    Rewriter.rewrite(
      demorgansRule <*
      doubleNegationRule <*
      valueNegationRule <*
      distributiveRule) (self)


// A main function to allow running the trafo example

import scala.language.implicitConversions

import dsldesign.expr.scala.adt.StringToExpression

@main def expressionToCNFMain =
  val self = !( ( !( !"D" & "V" ) &
      !( ( !"E" | ( ( "V" | "Y" ) | ( "C" & "H" ) ) ) & "L" ) ) &
      ( !"B" | !( "W" | ( ( ( ( !"H" & ( "Y" & "P" ) ) |
      !( "A" | "T" ) ) & "Y" ) & ( ( "F" & "F" ) | "W" ) ) ) ) )
  val converted = ExpressionToCNF.run (self)
  println (converted)
