// (c) dsldesign, berger, wasowski
// An ADT abstract syntax for .expr (and an evaluator)
package dsldesign.expr.scala.adt

// Abstract syntax

sealed abstract class Expression:
  def & (other: Expression): Expression = other match
    case True => this
    case _ => AND (this, other)

  def | (other: Expression): Expression = other match
    case False => this
    case _ => OR (this, other)

  def unary_! : Expression = NOT (this)


sealed abstract class BinaryExpression (
  val left: Expression, 
  val right: Expression
) extends Expression

sealed abstract class UnaryExpression (
  val expr: Expression
) extends Expression

case class NOT (e: Expression) extends UnaryExpression (e):
  override def toString = "!" + e

case class AND (l: Expression, r: Expression) extends BinaryExpression (l, r):
  override def toString = "( " + l + " & " + r + " )"

case class OR (l: Expression, r: Expression) extends BinaryExpression (l, r):
  override def toString = "( " + l + " | " + r+ " )"

case class Identifier (name: String ) extends Expression:
  override def toString = name

given StringToExpression: Conversion[String, Identifier] with
  def apply (s: String) = Identifier (s)

case object True extends Expression:
  override def & (other: Expression) = other
  override def unary_! = False
  override def toString = "TRUE"

case object False extends Expression:
  override def | (other: Expression) = other
  override def unary_! = True
  override def toString = "FALSE"

case class Configuration (
  val name: String,
  val identifierValues: Map[Identifier,Expression])



// An interpreter for .expr (an evaluator)

// This code can be run in Scala REPL:
//
// ./gradlew :dsldesign.expr.scala:repl --no-daemon --console plain
// import dsldesign.expr.scala.adt.*
//
// val env = List ("x" -> true, "y" -> false).toMap
// val exp = NOT (AND (Identifier ("x"), Identifier ("y")))
// eval (exp) (env)
//
// The same code is also placed as "test #1" in the test harness
// InterpreterSpec.scala

type Value = Boolean
type Env = Map[String, Value]

def eval (e: Expression) (env: Env): Option[Value] = e match
  case NOT (e) =>
    for  valE <- eval (e) (env) 
    yield ! valE

  case AND (l, r) => 
    for
      valL <- eval (l) (env)
      valR <- eval (r) (env)
    yield valL && valR

  case OR (l, r) => 
    for
      valL <- eval (l) (env)
      valR <- eval (r) (env)
    yield valL || valR

  case Identifier (name) =>
    env.get (name)

  case True =>
    Some (true)

  case False =>
    Some (false)
