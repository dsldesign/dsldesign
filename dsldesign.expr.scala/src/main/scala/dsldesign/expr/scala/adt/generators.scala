// (c) dsldesign, berger, wasowski
// A generator of random expressions for testing
package dsldesign.expr.scala.adt.generators

import scala.util.Random

import dsldesign.expr.scala.adt.*

def generateExpr (maxNumberOfIdentifiers: Int, maxNestingDepth: Int): Expression =
  val r = Random()
  val identifiers = (26 to (maxNumberOfIdentifiers + 25))
    .map { i =>
      (i % 26 + 65).toChar.toString + (if i/26 == 1 then "" else i/26) }
  subexp (maxNestingDepth, identifiers)

private def subexp (depth: Int, ids:Seq[String]): Expression =
  if depth <= 0 
    then Identifier (ids (Random.nextInt (ids.size)))
    else Random.nextInt (4) match
      case 0 => Identifier (ids (Random.nextInt (ids.size)))
      case 1 => NOT (subexp (depth - 1, ids))
      case 2 => AND (subexp (depth - 1, ids), subexp (depth - 1, ids))
      case 3 => OR (subexp (depth - 1, ids), subexp (depth - 1, ids))
