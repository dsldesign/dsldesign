// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.transforms

import scala.language.implicitConversions

import org.bitbucket.inkytonik.kiama.rewriting.Cloner.rewrite

import dsldesign.expr.scala.adt.StringToExpression
import dsldesign.expr.scala.adt.*

class ExpressionToCNFSpec extends
  org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.should.Matchers:

  val transform = ExpressionToCNF

  "test De Morgan's rule" in {
    val e = "a" | !("x" & !("y" & "z"))
    val res = rewrite (transform.demorgansRule) (e)
    res should equal ("a" | (!"x" | (!(!"y") & !(!"z"))))
  }

  "test double negation rule" in {
    val e = "a" | !(!"x")
    val res = rewrite (transform.doubleNegationRule) (e)
    res should equal ("a" | "x")
  }

  "test negation of values rule" in {
    val e = "a" & !True | "b" & !False
    val res = rewrite (transform.valueNegationRule) (e)
    res should equal ("a" & False | "b")
  }

  "test some expressions" in {
    val expressions = List (
      !(!("a" & "b") & "b"),
      ("S" & "V ") | (!"O" & !"A"),
      (((!"K" | "Z") & !"W") | !(((("L" & "B") | "Q") | !(("K" & ("F" & "L")) | "L")) | !"Q")),
      (!((!"B" | ("P" & (!("T" & "X") & ("B" & ((!"P" | "O") & (("G" & "B") | !"F")))))) &
        ("Y" | ("T" & "J"))) | "W"),
      !((!(!"D" & "V") & !((!"E" | (("V" | "Y") | ("C" & "H"))) & "L")) & (!(!"A2") | !("W" |
        ((((!"H" & ("Y" & "P")) | !("A" | "T")) & "Y") & (("F" & "F") | "W")))))
    )

    expressions.foreach(e =>
      isInCNF (transform.run (e)) should be (true)
    )
  }

  "test 50 randomly generated expressions" in {
    for i <- 1 to 50 do
      val e = generators.generateExpr( 26, 8 )
      isInCNF (transform.run (e)) should be (true)
  }

  /** The idea is to check that in each path to a leaf, there's no
    * conjunction after a disjunction anymore; and no disjunction or
    * conjunction after a negation.
    */
  def isInCNF (e: Expression): Boolean =
    def checkAllowedNodeTypesInSubtree 
      (node: Expression, conjAllowed: Boolean): Boolean = node match
        case OR (l, r) =>
          checkAllowedNodeTypesInSubtree (l, false) &&
          checkAllowedNodeTypesInSubtree (r, false)
        case AND (l, r) => conjAllowed &&
          checkAllowedNodeTypesInSubtree (l, true) &&
          checkAllowedNodeTypesInSubtree (r, true)
        case NOT (Identifier (_)) => true
        case NOT (_) => false
        case _ => true

    checkAllowedNodeTypesInSubtree (e, true)
