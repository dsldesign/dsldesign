// (c) dsldesign, berger, wasowski
// Example scenario tests for the expr interpreter in
// dsldesign.expr.scala
package dsldesign.expr.scala.adt

class InterpreterSpec extends
  org.scalatest.freespec.AnyFreeSpec,
  org.scalatest.matchers.should.Matchers,
  org.scalatest.Inside:

  "eval (!(x&&y)) (x->T, y->F) succeeds with true" in {
     val env = List ("x" -> true, "y" -> false).toMap
     val exp = NOT (AND (Identifier ("x"), Identifier ("y")))
     eval (exp) (env) should be (Some (true))
  }
