package dsldesign.fsm.xtend

import java.util.Collections
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

class XtendUtil {
	
	static def Resource loadFromXMI( String file ){
		val reg = Resource.Factory.Registry.INSTANCE
    	val m = reg.getExtensionToFactoryMap
    	m.put("xmi", new XMIResourceFactoryImpl )
		val resSet = new ResourceSetImpl
		resSet.getResource( URI.createURI( file ), true )
	}
	
	static def saveAsXMI( String file, EObject rootElement ){
		val resSet = new ResourceSetImpl
		val res = resSet.createResource( URI.createURI( file ) )
	    res.getContents().add( rootElement )
	    res.save(Collections.EMPTY_MAP )
	}
	
}