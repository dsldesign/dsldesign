// (c) dsldesign, berger, wasowski
// An in-place transformation implemented in Xtend. Translates a finite state
// machine to a Petri net. Run using ToPetriNetMain.xtend

package dsldesign.fsm.xtend

import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.State
import dsldesign.petrinet.Place

import org.eclipse.emf.common.util.EList
import dsldesign.fsm.Transition

class ToPetriNet {
	
	val static pFactory = dsldesign.petrinet.PetrinetFactory.eINSTANCE

	def dsldesign.petrinet.Place convertState( State s ){
		pFactory.createPlace => [
			name = s.name
			tokenNo = 0 ]
	}
	
	def dsldesign.petrinet.Transition convertTransition( Transition t, EList<Place> places ){
		pFactory.createTransition => [
			input = t.input
			fromPlace += places.filter[ x | x.name == t.source.name ]
			toPlace += places.filter[ x | x.name == t.target.name ] ]
	}

	def dsldesign.petrinet.PetriNet convertStateMachine( FiniteStateMachine fsm ){
		pFactory.createPetriNet => [
			name = fsm.name
			places += fsm.states.map[s | convertState( s ) ]
			transitions += fsm.states.map[ s | s.leavingTransitions ].flatten.
			               map[ s | convertTransition( s, places ) ]
			
			// add a token to the initial state
			places.findFirst[ name == fsm.initial.name ].tokenNo = 1
			
			// for each end state generate a transition that throws a token away
			fsm.endStates.forEach[ s | 
				val p = places.findFirst[ name == s.name ]
				val t = pFactory.createTransition => [ fromPlace += p; input = "" ]
				p.outgoingTransitions += t
				transitions += t ] ]
	}	

	def dsldesign.petrinet.Model run( dsldesign.fsm.Model m ){
		pFactory.createModel => [
			petrinets += m.machines.map[ convertStateMachine ] ]
	}	
			
	private def endStates( FiniteStateMachine m ){
		m.states.filter[ s | s.leavingTransitions.empty ]
	}

}
