// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.xtend

import java.io.File
import java.io.PrintWriter
import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.FsmPackage
import dsldesign.fsm.Model
import dsldesign.petrinet.PetrinetPackage

class ToJavaCodeMain {
	
	def static void main (String[] args) {
		
		// register our meta-model package for abstract syntax
		FsmPackage.eINSTANCE.eClass
		PetrinetPackage.eINSTANCE.eClass
		
		// get our state machine from the abstract syntax (XMI file)
		val resource = XtendUtil.loadFromXMI( "../dsldesign.fsm/test-files/coffeemachine.xmi" )

		/* The call to get(0) below gives you the first model root. 
		// If you open a directory instead of a file, 
		// you can iterate over all models in it, 
		// by changing 0 to a suitable index */
		val fsmModel = resource.contents.get(0) as Model
		
		// we run the transformation, which changes m
		fsmModel.machines.forEach[ FiniteStateMachine fsm |
			val code = ToJavaCode.compileToJava( fsm )
			var pw = new PrintWriter( "xtend-gen/" + fsm.name + ".java")
			pw.println( code )
			pw.close
			
			val dot = ToJavaCode.compileToDot( fsm )
			pw = new PrintWriter( "xtend-gen/" + fsm.name + ".dot")
			pw.println( dot )
			pw.close
			var cmd = #["dot", "-Tpdf", fsm.name + ".dot", "-o", fsm.name + ".pdf"]
			Runtime.runtime.exec(cmd, null, new File( "xtend-gen") )
		]
		
	}
	
}


