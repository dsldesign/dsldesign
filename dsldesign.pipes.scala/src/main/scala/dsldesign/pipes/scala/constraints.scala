// (c) dsldesign, wasowski, tberger
// Example constraints implemented for the Pipes models in Scala
package dsldesign.pipes.scala.constraints

import scala.jdk.CollectionConverters.*
import dsldesign.scala.emf.*
import dsldesign.pipes.*

val invariants: List[Constraint] = List (

  // This constraint is not satisfied because internal nodes are both sinks and
  // sources in our metamodel, so we have two sources and two sinks in the
  // instance in dsldesign.pipes/test-files/test-00.xmi
  inv[PipesModel] { m =>
    m.getNodes.asScala.filter { _.isInstanceOf[Source] }.size == 1 &&
      m.getNodes.asScala.filter { _.isInstanceOf[Sink] }.size == 1
  },

  // add more constraints here
)
