// (c) dsldesign, wasowski, tberger
package dsldesign.expr.java;

import java.util.Map;
import java.util.HashMap; 

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import dsldesign.expr.*;
import dsldesign.scala.emf.emf$package$;

@DisplayName ("Ecore Interpreter: Test cases")
class InterpreterTest {

  ExprFactory factory = ExprFactory.eINSTANCE;

  @BeforeAll
  static void before () { ExprPackage.eINSTANCE.eClass (); }

  Expression loadExpr (String name) {
     return emf$package$.MODULE$.loadFromXMI
      ("../dsldesign.expr/test-files/" + name + ".xmi");
  }

  // Create an empty environment 
  Map<String, Boolean> env0 () {
     return new HashMap<String, Boolean> ();
  } 

  boolean testBool (String file, boolean x, boolean y)
  {
    Expression e = loadExpr (file);
    var env = env0 ();

    env.put ("x", x);
    env.put ("y", y);
    Boolean result = 
      assertDoesNotThrow (() -> Interpreter.eval (env, e));
    return result;
  }

  @Test
  @DisplayName ("[x||!x] test-01.xmi is true for x=true and for x=false")
  void test_01() 
  { 
    assertEquals (testBool("test-01", true, true), true);
    assertEquals (testBool("test-01", false, true), true);
    assertEquals (testBool("test-01", true, false), true);
    assertEquals (testBool("test-01", false, false), true);
  }


  @Test
  @DisplayName ("[x&&!x] test-02.xmi is false for x=true and for x=false")
  void test_02() 
  { 
    assertEquals (testBool("test-02", true, true), false);
    assertEquals (testBool("test-02", false, true), false);
    assertEquals (testBool("test-02", true, false), false);
    assertEquals (testBool("test-02", false, false), false);
  }


  @Test
  @DisplayName ("[x&&y] test-03.xmi is true for x=y=true and false otherwise")
  void test_03() 
  { 
    assertEquals (testBool("test-03", true, true), true);
    assertEquals (testBool("test-03", false, true), false);
    assertEquals (testBool("test-03", true, false), false);
    assertEquals (testBool("test-03", false, false), false);
  }
}
