// (c) dsldesign, wasowski, tberger
package dsldesign.expr.java;

class RuntimeError extends RuntimeException {
  public RuntimeError (String msg) { this.msg = msg; }
  public final String msg;
  public String toString () { return msg; }
}
