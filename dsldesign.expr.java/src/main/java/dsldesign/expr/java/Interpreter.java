// (c) dsldesign, wasowski, tberger
package dsldesign.expr.java;

import java.util.Map;
import java.util.HashMap;

import dsldesign.expr.*;

import dsldesign.expr.util.ExprSwitch;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

class Interpreter {

  static public Boolean eval (Map<String, Boolean> env, Expression expr) 
    throws RuntimeError
  { return new EvalSwitch (env).doSwitch (expr); }

  static class EvalSwitch extends ExprSwitch<Boolean> {
    
    private final Map<String, Boolean> env;

    public EvalSwitch (Map<String, Boolean> env)
    {  
      super ();
      this.env = env;
    }

    @Override public Boolean caseIdentifier (Identifier expr)
    {
       Boolean result = this.env.get (expr.getName());
       if (result == null)
         throw new RuntimeError ("Access to undefined variable '" 
             + expr.getName() + "'");
       return result;
    }

    public Boolean caseAND (AND expr)
    {
        Boolean valL = this.doSwitch (expr.getLeft ());
        Boolean valR = this.doSwitch (expr.getRight ());
        return valL && valR;
    }

    public Boolean caseOR (OR expr)
    {
        Boolean valL = this.doSwitch (expr.getLeft ());
        Boolean valR = this.doSwitch (expr.getRight ());
        return valL || valR;
    }

    public Boolean caseNOT (NOT expr)
    {
        return !this.doSwitch (expr.getExpr ());
    }

    @Override public Boolean defaultCase (EObject ignore) throws RuntimeError
    { 
      throw new RuntimeError ("Internal Error (Should not happen). " +
          "Attempted to evaluate object " + ignore); 
    }
  }

}
