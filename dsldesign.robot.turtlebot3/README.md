# dsldesign.robot.turtlebot3

## What is in here?

In this directory we implement (manually) an example controller for
turtlebot3, to serve as an example for code generation for turtlebot
from the robot language. This controller can be substituted with the
code generated later.

*Note:* The convention of directory names in our project clashes with
ROS package name conventions, so the directory name is mapped to
`dsldesign_robot_turtlebot3` in ROS, and this name is used for the ROS
package (in the docker container, see below).

## How to run the reference implementation

The included Dockerfile includes ROS2 foxy distribution with the
webots simulator. This project tree is mounted inside the container.

The docker container can be build and started with `bash start.sh`
on Linux.  Look into the script if it fails on your machine.  The idea
is to launch ROS and the Webots simulator in the container, so that
you can experiment without having the robot hardware.

You can launch webots with turtlebot3 by pulling out the following
command from the bash history in the container (arrow up after opening
the container):
```bash
ros2 launch webots_ros2_turtlebot robot_launch.py
```
Sometimes (on slower PCs?) the differential drive controller fails to
start. A succesful start can be confirmed by checking whether the
topic `/cmd_vel` exists with `ros2 topic list`. If it
failed you can start it manually in another terminal connected to the
running container with of the following commands:
```
ros2 run controller_manager spawner.py diffdrive_controller
```
(see below how to attach new shell sessions to a running docker
container)

Then we need to launch the navigation setup:
```
ros2 launch turtlebot3_navigation2 navigation2.launch.py use_sim_time:=true map:=$(ros2 pkg prefix webots_ros2_turtlebot --share)/resource/turtlebot3_burger_example_map.yaml
```
The above line is also included in the sell history, so that you can
bring it back easily.  Ideally all these steps should be replaced with
a launch script one day...

Now to start the controller that realizes the random walk:
```
ros2 run dsldesign_robot_turtlebot3 controller
```
To simulate a clap event issue the following from another terminal
than the controller process:

```
ros2 topic pub --once "/clap" example_interfaces/msg/Bool '{"data": true}'
```
This should activate the event for going back to the starting position
(simulating a docking position).

(It would be fun to add a component that listens to the laptop
microhpne for claps and issues this command.)

# Opening more terminal sessions with Docker

To connect another terminal to the running session, first get the name with:
```
$ docker ps
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS               NAMES
209d72a09c2d        dsldesign/turtlebot3   "/ros_entrypoint.sh …"   11 minutes ago      Up 11 minutes                           naughty_snyder
```
then
```
docker exec -it <container name> /bin/bash
```

