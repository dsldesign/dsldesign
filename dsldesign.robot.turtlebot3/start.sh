#!/bin/bash
# A script to run the simulator in a docker container (works on Ubuntu Linux)

docker build ./ --tag dsldesign/turtlebot3

docker run --device=/dev/dri:/dev/dri \
  --env DISPLAY=$DISPLAY \
  --hostname $HOSTNAME \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --volume $HOME/.Xauthority:/home/dsldesign/.Xauthority \
  --device /dev/snd \
  --env PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
  --volume ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
  --group-add $(getent group audio | cut -d: -f3) \
  --interactive \
  --tty \
  --volume $(pwd):/home/dsldesign/ros_ws/src/dsldesign_robot_turtlebot3 \
  --volume $(pwd)/../dsldesign.robot/:/home/dsldesign/ros_ws/src/dsldesign_robot \
  dsldesign/turtlebot3
