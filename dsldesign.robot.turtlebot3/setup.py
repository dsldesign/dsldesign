from setuptools import setup

package_name = 'dsldesign_robot_turtlebot3'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='http://dsl.design',
    maintainer_email='wasowski.andrzej@gmail.com',
    description='Entrypoints for controllers for turtlebot3 in Python (a random walk)',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'controller = dsldesign_robot_turtlebot3.controller:main',
            'interpreter = dsldesign_robot_turtlebot3.interpreter:main',
            'generated_controller = dsldesign_robot_turtlebot3.generated_controller:main',
        ],
    },
)
