#!/usr/bin/env python
# (c) dsldesign, wasowski, berger

# Known issues: no good detection that AcDock has completed is detected, so 
# it only works if this is the terminating action in the model (the interpreter
# can handle the general case, but more ROS magic is needed to make this to work)

# This code does not depend on ROS directly. All dependencies are encapsulated
# in turtlebot.py. This module is only concerned with control behavior.  This
# also makes it easier to turn it an interpreter or code generator

import dsldesign_robot_turtlebot3.turtlebot 
from dsldesign_robot_turtlebot3.turtlebot import TurtleBotPlatform

import random

from pyecore.resources import ResourceSet, URI
from pyecore.utils import DynamicEPackage


class Interpreter(TurtleBotPlatform):

    def __init__(self, executor) -> None:

        super().__init__("interpreter", executor)
        # Load the robot meta-model
        self.resource_set = ResourceSet()
        self.resource = self.resource_set.get_resource(
                URI('src/dsldesign_robot/model/robot.ecore'))
        robot_mmodel = self.resource.contents[0]

        self.Robot = DynamicEPackage(robot_mmodel)
        # Register robot ast the meta-model (needed)
        self.resource_set.metamodel_registry[robot_mmodel.nsURI] = robot_mmodel
        # Load the model to interpret, the path to a model would
        # normally not be hard-coded (we do for simplicity).
        self.model = self.load_instance(
                "src/dsldesign_robot/test-files/random-walk.robot.xmi")

        # Model initialization / kick-off 
        # (the top-level model element is a mode)
        self.activate(self.model)

    def load_instance (self, fname):
        """A helper function to load robot instances. We read in the
        model in XMI format, to decrease dependencies between the
        various small projects in the book. However, at this point a
        parser could've been invoked, and concrete syntax loaded
        instead."""
        self.info(f'Loading "{fname}"...')
        self.resource = self.resource_set.get_resource(URI (fname))
        return self.resource.contents[0]

    def activate(self, mode) -> None:
        """Make a mode active and schedule executing its actions, and
        activation of nested modes."""
        self.mode = mode
        self.mode_pc = len(self.mode.actions)
        self.info(f'Activated: {self.mode.name}')

    def evaluate_expr(self, expr):
        """Evaluate an expression (Expressions are embedded in 
        action lines in .robot language)."""
        if isinstance(expr, self.Robot.RndI):
            return random.randrange(0, 2000) / 1000.0
        elif isinstance(expr, self.Robot.CstI):
            return expr.value
        elif isinstance(expr, self.Robot.Minus):
            return - self.evaluate_expr (expr.aexpr)
        elif isinstance(expr, self.Robot.BinExpr):
            left = self.evaluate_expr (expr.left)
            right = self.evaluate_expr (expr.right)
            if expr.ope == '+': return left + right
            elif expr.ope == '-': return left - right
            elif expr.ope == '*': return left * right
            elif expr.ope == '\\': return left / right
            else: assert False, expr.ope

    def execute_action(self, action) -> None:
        """Execute a given action, possibly evaluating expressions of 
        its parameters."""

        self.info(f'Execute {action.eClass.name}')

        if isinstance(action, self.Robot.AcMove):
            self.info(f'action.forward={action.forward}')
            direction = 1.0 if action.forward == True else -1.0
            if action.speed:
                velocity = self.evaluate_expr(action.speed) * 0.008 * direction
            else:
                velocity = 0.1 * direction

            if action.duration:
                self.engage(velocity, float(self.evaluate_expr(action.duration)))
            else: 
                self.engage(velocity)

        elif isinstance(action, self.Robot.AcTurn):
            self.random_rotation()

        elif isinstance(action, self.Robot.AcDock):
            self.return_to_base()

        else:
            assert False, action

    def active_modes(self):
        """List modes presently active, starting with the outermost one."""
        s = self.mode
        result = []
        while True:
            result.append(s)
            if not s.eContainer(): break
            s = s.eContainer()
        return reversed(result)

    def run(self):
        """Run the main interpreter loop that executes reactions to external
        events."""
        while True:

            # allow parallel callbacks to spin in
            self.executor.spin_once(0.0)

            # The constant below is essentially arbitrary, just to avoid busy
            # waiting.
            if self.tm_latch in self.timers:
                self.executor.spin_once(1.0/self.FREQ)
                continue

            if self.mode_pc > 0:
                self.execute_action(self.mode.actions[-self.mode_pc])
                self.mode_pc = self.mode_pc - 1
                continue

            try:
                initial = next(filter(lambda m: m.initial, self.mode.modes))
                self.activate(initial)
                continue
            except StopIteration:
                pass

            # execute reactions

            reacted = False
            for m in self.active_modes():
                if reacted: break
                for r in m.reactions:

                    if (self.ev_clap 
                            and r.trigger == self.Robot.Event.EV_CLAP):
                        self.ev_clap = False
                        if r.target: self.activate(r.target)
                        reacted = True
                        break

                    elif (self.ev_obstacle 
                            and r.trigger == self.Robot.Event.EV_OBSTACLE):
                        self.ev_obstacle = False
                        reacted = True
                        if r.target: self.activate(r.target)
                        break
                    
                    elif self.mode.continuation: 
                        self.activate(self.mode.continuation)
                        reacted = True
                        break

            # The above design enforces a possibly unexpected behavior: if two
            # events happen almost simultanously the first one will be
            # processed in the current state, The second one will be processed
            # after reacting to the first one, possibly after a state change.
            # Not an ideal semantics, but we do not want to make this
            # interpreter larger.

            # Consume events inactive in current state.
            self.ev_clap = False
            self.ev_obstacle = False

def main(args = None):
    dsldesign_robot_turtlebot3.turtlebot.run(
            args, lambda executor: Interpreter(executor))

if __name__ == '__main__': 
    main()
