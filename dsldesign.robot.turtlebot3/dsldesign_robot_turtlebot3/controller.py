#!/usr/bin/env python
# (c) dsldesign, wasowski, berger

# A reference implementation of random-walk created as a preparation exercise
# before implementing the interpreter and the code generator. 

import dsldesign_robot_turtlebot3.turtlebot 
from dsldesign_robot_turtlebot3.turtlebot import TurtleBotPlatform

import random

# We use a scheme that assigns prime numbers as identifiers to modes. Each mode
# gets its own prime number, and it is multiplied by prime numbers of all its
# parents to create the mode ID.
# This way we can check for parent relationships at runtime using modulo division.
_RANDOM_WALK = 1
_MOVING_FORWARD = 1*2
_AVOID = 1*3
_SHUT_DOWN = 1*5

_MODE_NAME = {
  _RANDOM_WALK : 'RandomWalk',
  _MOVING_FORWARD : 'MovingForward',
  _AVOID : 'Avoid',
  _SHUT_DOWN : 'ShutDown',
}


class RandomWalk(TurtleBotPlatform):
    """A reference implementation of the Random Walk example from Chapter 2."""

    def __init__(self, executor):
        super().__init__("random_walk", executor)
        self.activate(_RANDOM_WALK)

    def activate(self, mode):
        self.mode = mode
        self.mode_pc = 0
        self.info(f'Activated: {_MODE_NAME[self.mode]}')

    def active(self, mode):
        return self.mode % mode == 0

    def execute_RANDOM_WALK(self):
        self.info('RANDOM_WALK[initializing submode]')
        self.activate(_MOVING_FORWARD)
        return True

    def execute_MOVING_FORWARD(self):
        if self.mode_pc == 0:
            self.info('MOVING_FORWARD[0]')
            direction = 1.0
            model_speed = 10
            velocity = model_speed * 0.008 * direction
            self.engage(velocity)
            self.mode_pc = self.mode_pc + 1
            return True
        return False

    def execute_AVOID(self):
        if self.mode_pc == 0:
            self.info('AVOID[0]')
            direction = -1.0
            velocity = 0.1 * direction
            self.engage(velocity, float(2))
            self.mode_pc = self.mode_pc + 1
            return True
        if self.mode_pc == 1:
            self.info('AVOID[1]')
            self.random_rotation()
            self.mode_pc = self.mode_pc + 1
            return True
        return False

    def execute_SHUTDOWN(self):
        if self.mode_pc == 0:
            self.info('SHUTDOWN[0]')
            self.return_to_base()
            self.mode_pc = self.mode_pc + 1
            return True
        return False

    def run(self):
        while True:
            # allow parallel callbacks to spin in
            self.executor.spin_once(0.0)

            # The constant below is essentially arbitrary, 
            # just to avoid busy waiting.
            if self.tm_latch in self.timers:
                self.executor.spin_once(1.0/self.FREQ)
                continue

            # actions and sub-mode activations

            if self.mode == _RANDOM_WALK and self.execute_RANDOM_WALK():
                continue
            if self.mode == _MOVING_FORWARD and self.execute_MOVING_FORWARD():
                continue
            if self.mode == _AVOID and self.execute_AVOID():
                continue
            if self.mode == _SHUT_DOWN and self.execute_SHUTDOWN():
                continue

            # reactions (topologically sorted, so that top-mode reactions
            # override the low-mode reactions)

            if self.active(_RANDOM_WALK):
                if self.ev_clap:
                    self.info('Reacting to clap!')
                    self.ev_clap = False
                    self.activate(_SHUT_DOWN)
                    continue

            if self.active(_MOVING_FORWARD):
                if self.ev_obstacle:
                    self.info('Reacting to an obstacle!')
                    self.ev_obstacle = False
                    self.activate(_AVOID)
                    continue

            # continuations

            if self.mode == _AVOID:
                self.activate(_MOVING_FORWARD)
                continue


def main(args=None):
    dsldesign_robot_turtlebot3.turtlebot.run(
            args, lambda executor: RandomWalk(executor))


if __name__ == '__main__':
    main()
