#!/usr/bin/env python
# (c) dsldesign, wasowski, berger

import random
import threading
from typing import List

import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient
from rclpy.qos import QoSDurabilityPolicy, QoSHistoryPolicy, QoSReliabilityPolicy
from rclpy.qos import QoSProfile

from example_interfaces.msg import Bool
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseWithCovarianceStamped 
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import LaserScan

from nav2_msgs.action import NavigateToPose

class TurtleBotPlatform(Node):
    """Framework Runtime + API implementation
       Language / Framework callbacks"""

    def __init__(self, name, executor):

        super().__init__(name)

        self.FREQ = 10.0 # Hz
        self.executor = executor

        # Framework event listeners and timers
        self.cmd_vel = self.create_publisher(Twist, '/cmd_vel', 10)
        self.initial_pose = self.create_publisher(PoseWithCovarianceStamped, 
                '/initialpose', 3)
        self.scan_listener = self.create_subscription(LaserScan, '/scan',
                self.scanner_callback, 1)
        self.clap_listener = self.create_subscription(Bool, '/clap',
                self.clap_callback, 10)
        self.tm_control_loop = self.create_timer(1.0/self.FREQ, self.msg_pulse_callback)

        # Internal framework states
        self.latched_msg = Twist()
        self.mode = []
        self.tm_latch = None
        self.ev_clap = False
        self.ev_obstacle = False

        # Configure navigation 2

        amcl_pose_qos = QoSProfile(
          durability=QoSDurabilityPolicy.TRANSIENT_LOCAL,
          reliability=QoSReliabilityPolicy.RELIABLE,
          history=QoSHistoryPolicy.KEEP_LAST,
          depth=1)

        self.nav_to_pose_client = ActionClient(self, NavigateToPose, 
                'navigate_to_pose') #, callback_group = self.cgroup)
        pose = PoseWithCovarianceStamped()
        pose.header.frame_id = "map"
        self.initial_pose.publish(pose)

    def info(self, msg: str) -> None:
        """A convenience function for reporting progress using ROS 
        logging facility."""
        self.get_logger().info(msg)

    def latch(self, duration: float = -1.0) -> None:
        """'Latch' the message for non-preemptive duration seconds, 
           or if duration -1, latch it for infinity but make preemptive."""
        if duration >= 0.0:
            self.tm_latch = self.create_timer(duration, self.tm_latch_callback)
            self.info(f"Latched {swist(self.latched_msg)} only for {duration}s")
        else:
            self.info(f"Latched {swist(self.latched_msg)}")

    def tm_latch_callback(self) -> None:
        """Discover that a message has timed out, and 'unlatch' it."""
        self.latched_msg = Twist()
        self.destroy_timer(self.tm_latch)

    def msg_pulse_callback(self) -> None:
        """Repeatedly send latched message to /cmd_vel, if there is one."""
        if self.tm_latch in self.timers or self.latched_msg != Twist():
            self.cmd_vel.publish(self.latched_msg)

    def scanner_callback(self, msg: LaserScan) -> None:
        """Detect an obstacle with reasonable range of sensing in front"""
        distance = min(msg.ranges[120:240])
        self.ev_obstacle = self.ev_obstacle or (distance <= 0.16)

    def clap_callback(self, msg: Bool) -> None:
        """Called when a clap is observed (mocked)"""
        self.info('CLAP!!!')
        self.ev_clap = msg.data

    # Operations on the robot

    def random_rotation(self, duration: float = -1.0) -> None:
        self.latched_msg = Twist()
        self.latched_msg.angular.z = -1.0 if bool(random.getrandbits(1)) else 1.0
        if duration == -1.0:
            duration = random.randrange(500, 1900) / 1000.0
        self.latch(duration)

    def engage(self, velocity: float, duration: float = -0.7) -> None:
        self.latched_msg = Twist()
        self.latched_msg.linear.x = velocity
        self.latch(duration)

    def stop(self, duration: float = -1.0) -> None:
        self.latched_msg = Twist()
        self.latch(duration)

    def back_off(self, duration: float = 0.7) -> None:
        self.latched_msg = Twist()
        self.latched_msg.linear.x = -0.8
        self.latch(duration)

    def return_to_base(self) -> None:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        self.latched_msg = Twist()
        self._go_to_pose(pose)
 
    # The following inspired from 
    # https://github.com/ros-planning/navigation2/issues/2283#issuecomment-853456150
    # (credits: Steve Mecenski)
    def _go_to_pose(self, pose: PoseWithCovarianceStamped) -> None:
        goal_msg = NavigateToPose.Goal()
        goal_msg.pose = pose
        self.info(f"Navigating to {pose.pose.position.x} {pose.pose.position.y}")
        # I had some issues that a synchronous call was hanging even if the navigation 
        # failed (so no way to recover)
        navigation_future = self.nav_to_pose_client.send_goal_async(goal_msg)
        # Unclear why this returns immediately, which means we have no good way
        # right away to use the AcDock action in antyhing else buta final state
        self.executor.spin_until_future_complete(navigation_future)

def swist(m) -> str:
    """Format a 2D twist message a string for printing"""
    return f"({m.linear.x},{m.linear.y})/{m.angular.z}" if m else "N/A"

def run(args, mkController):
    rclpy.init(args = args)
    try:
        executor = rclpy.executors.SingleThreadedExecutor()
        controller = mkController(executor)
        executor.add_node(controller)
        try:
            controller.run()
        finally:
            executor.shutdown()
            controller.destroy_node()
    finally:
        rclpy.shutdown()
