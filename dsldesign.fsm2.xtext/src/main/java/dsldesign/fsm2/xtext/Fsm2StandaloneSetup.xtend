/*
 * generated by Xtext 2.12.0
 */
package dsldesign.fsm2.xtext


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class Fsm2StandaloneSetup extends Fsm2StandaloneSetupGenerated {

	def static void doSetup() {
		new Fsm2StandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
