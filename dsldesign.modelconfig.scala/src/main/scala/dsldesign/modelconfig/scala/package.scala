// (c) dsldesign, berger, wasowski
package dsldesign.modelconfig.scala

import java.io.{File, FileInputStream}
import java.util.Properties

import scala.jdk.CollectionConverters.*

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.{EModelElement, EObject, EPackage}
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl

import dsldesign.expr.xtext.scala.*
import dsldesign.expr.xtext.{ExprParseHelper, ExprStandaloneSetup}
import dsldesign.expr.*
import dsldesign.fsmpl.FsmplPackage
import dsldesign.scala.emf
import dsldesign.xtext.scala.parse

def deriveModel (epackage: EPackage, configurationPropertiesFile: File): Unit =
  val properties = Properties ()
  properties.load (FileInputStream (configurationPropertiesFile))
  deriveModel (epackage, properties)

// A very simple configuration value parsing
def deriveModel (epackage:EPackage, conf: Properties): Unit =
  val c: List[(String, Boolean)] = conf.asScala
    .map { (k, v) => (k.toString, v.toString.equalsIgnoreCase ("true")) }
    .toList
  deriveModel (epackage, createConfiguration ("", c))

def deriveModel (epackage:EPackage, conf: Map[String, Boolean]): Unit =
  deriveModel (epackage,
    createConfiguration ("", conf.toList))

def deriveModel (epackage: EPackage, conf: Configuration): Unit =
  val c = EcoreUtil.getAllProperContents[EObject] (epackage, false)
  for item <- c.asScala
  do item match
    case e: EModelElement =>
       e.getEAnnotations.asScala
         .find { _.getDetails.containsKey ("condition") }
         .foreach { annotation => // at most one
           val cond = annotation.getDetails.get ("condition")
           if evaluateCondition (cond, conf)
             // include element, i.e., just remove annotation
             then EcoreUtil.delete (annotation)
             // do not include element, i.e., remove the element
             else EcoreUtil.delete (e)
         }
    case _ =>

private lazy val setup = ExprStandaloneSetup ()

def evaluateCondition (cond: String, conf: Configuration): Boolean =
  given org.eclipse.xtext.ISetup = setup
  eval (cond.parse[Expression].get, conf) // crash on a parse error


// A main function to allow running the example with
// ./gradlew :dsldesign.modelconfig.scala:run

@main def main =
  // Register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("ecore", EcoreResourceFactoryImpl ())

  // Register the package magic (impure)
  FsmplPackage.eINSTANCE.eClass
  ExprPackage.eINSTANCE.eClass

  val epackage: EPackage = emf.loadFromXMI ("../dsldesign.fsmpl/model/fsmpl.ecore")
  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content = EcoreUtil.getAllProperContents[EObject] (epackage, false)

  val propFile = "../dsldesign.fsmpl/test-files/configuration1.properties"
  deriveModel (epackage, File (propFile))
  emf.saveAsXMI ("../dsldesign.fsmpl/test-files/configured/fsmpl1.ecore") (epackage)
