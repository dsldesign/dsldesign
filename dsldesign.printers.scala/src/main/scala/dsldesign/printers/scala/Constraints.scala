// (c) dsldesign, wasowski, tberger
package dsldesign.printers.scala

import scala.jdk.CollectionConverters.*
import dsldesign.scala.emf.*
import dsldesign.printers.*

/* We put constraints for all the six meta-models on one list for simplicity
 * (the context type will ensure that the constraints are only applied to
 * instances of the right models anyways -- and we don't have a performance
 * issue in this exercise)
 */

val invariants = List[(String,Constraint)] (

  "1 Printer is mandatory" -> inv[t1.PrinterPool] { _.getPrinter != null },
  // satisfied by t1-01, t1-03, violated by t1-00 and t1-02
  // Test cases in dsldesign.printers/test-files.

  "2 Fax requires a printer" -> inv[t1.PrinterPool] { self =>
    self.getFax != null implies self.getPrinter != null },
  // satisfied by t1-00, t1-01, t1-03, violated by t1-02

  "3 If you have a fax then you have a printer" ->
    inv[t1.Fax] { _.getPool.getPrinter != null },
  // satisfied by t1-00, t1-01, t1-03, violated by t1-02

  "4 A printer pool with a fax has printer, and with a copier has scanner+printer"
    -> inv[t2.PrinterPool] { self =>
      (self.getFax != null implies self.getPrinter != null) &&
      (self.getCopier != null implies
         self.getScanner != null && self.getPrinter != null)
  },
  // satisfied by: t2-00, t2-02, violated by t2-01

  "5 PrinterPool’s minimum speed must be 300 lower than its regular speed" ->
    inv[t3.PrinterPool] { self => self.getMinSpeed == self.getSpeed - 300 },
  // satisfied by: none, violated by t3-00, t3-01

  "6 Every color printer has a colorPrinterHead" ->
    inv[t4.Printer] { self => self.isColor implies self.getHead != null },
  // satisfied by t4-01, violated by t4-00

  "7 A color-capable printer pool contains at least one color-capable printer" ->
    inv[t5.PrinterPool] { self =>
      self.isColor implies self.getPrinter.asScala.exists (_.isColor) },
  // satisfied by t5-01, violated by t5-00

  "8 If a Printer pool contains a color scanner then it contains a color printer" ->
    inv[t6.PrinterPool] { self =>
      self.getScanner.asScala.exists (_.isColor) implies
        self.getPrinter.asScala.exists (_.isColor) },
  // satisfied by t6-01, t6-02 violated by t6-00

  "9 If a printer pool has a color scanner, all its printers are color printers" ->
    inv[t6.PrinterPool] { self =>
      self.getScanner.asScala.exists (_.isColor) implies
        self.getPrinter.asScala.forall (_.isColor) },
  // satisfied by t6-02 violated t6-00, t6-01

  "10 There is at most one color printer in any pool" ->
    inv[t6.PrinterPool] { _.getPrinter.asScala.filter {_.isColor}.size <= 1 }
  // satisfied by t6-00, t6-01 violated t6-02
)
