// (c) dsldesign, wasowski, tberger
// This is the main driver for printer constraints
package dsldesign.printers.scala

import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

@main def main =
  // Register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // Register the the meta-model packages (quite a few in this exercise, impure)
  dsldesign.printers.t1.T1Package.eINSTANCE.eClass
  dsldesign.printers.t2.T2Package.eINSTANCE.eClass
  dsldesign.printers.t3.T3Package.eINSTANCE.eClass
  dsldesign.printers.t4.T4Package.eINSTANCE.eClass
  dsldesign.printers.t5.T5Package.eINSTANCE.eClass
  dsldesign.printers.t6.T6Package.eINSTANCE.eClass

  // Load the example models
  val cases = List("t1-00", "t1-01", "t1-02", "t1-03", "t2-00", "t2-01",
    "t2-02", "t3-00", "t3-01", "t4-00", "t4-01", "t5-00", "t5-01", "t6-00",
    "t6-01", "t6-02")
  val uris = cases.map { c =>
    URI.createURI (s"../dsldesign.printers/test-files/$c.xmi") }
  val ress = uris.map { uri =>
    ResourceSetImpl ().getResource (uri, true) }

  // Check constraints and print results
  for
    (f,res) <- cases zip ress
  do
    val content = EcoreUtil.getAllContents[EObject] (res, false).asScala.toList
    val relevant = invariants.filter ( _._2 appliesToAny content )
    val (labels, predicates) = relevant.unzip
    val results = predicates.map { p => content.forall { p check _ } }
    val msgs = results.map { if _ then "pass" else "FAIL" }

    println (s"\nCase $f: ")
    for (l,m) <- labels zip msgs do println (s"  [$m] $l")
