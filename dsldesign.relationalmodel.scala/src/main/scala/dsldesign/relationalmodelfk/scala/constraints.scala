// (c) dsldesign, wasowski, tberger
package dsldesign.relationalmodelfk.scala.constraints

import scala.jdk.CollectionConverters.*

import dsldesign.relationalmodelfk.*
import dsldesign.scala.emf.*

val invariants: List[Constraint] = List (

    inv[Root] { _ => true } // modify this constraint
)
