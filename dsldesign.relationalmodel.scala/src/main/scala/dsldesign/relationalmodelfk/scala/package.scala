// (c) dsldesign, wasowski, tberger
// Example constraints implemented for the Relational models in Scala
// This is the main runner for the constraints example
package dsldesign.relationalmodelfk.scala

import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*
import dsldesign.relationalmodelfk.RelationalmodelfkPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

@main def main =
  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // register the package magic (impure)
  dsldesign.relationalmodelfk.RelationalmodelfkPackage.eINSTANCE.eClass ()

  // // load the XMI file <-- change file name below
  val uri: URI = URI.createURI ("../dsldesign.relationalmodel/test-files/test-00.xmi")
  val resource: Resource = ResourceSetImpl ().getResource (uri, true)

  val content: Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).asScala

  if content.forall { eo => constraints.invariants.forall (_ check eo) } 
    then println ("All constraints are satisfied!")
    else println ("Some constraint is violated!")
