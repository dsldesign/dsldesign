// (c) dsldesign, tberger, wasowski
// An exogeneous transformation implemented directly in Scala (no special
// frameworks).  Translates a simple class model to a relational model.
package dsldesign.relationalmodelfk.scala.transforms

import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*
import dsldesign.{classmodel, relationalmodelfk => RN}

object ClassModelToRelationalModel extends CopyingTrafo[classmodel.Root, RN.Root]:

  val rFactory = RN.RelationalmodelfkFactory.eINSTANCE

  def convertAttribute (self: classmodel.IntegerAttribute): RN.IntegerColumn =
    rFactory.createIntegerColumn before {
      _.setName (self.getName)
    }

  def convertClass (self: classmodel.Class): RN.Table =
    rFactory.createTable before { t =>
      t.setName (self.getName)

      val key = rFactory.createIntegerColumn before { _.setName ("id") }
      t.getPrimaryKeys.add (key)
      t.getColumns.add (key)

      t.getColumns.addAll (self.getAttribute.asScala.map (convertAttribute).asJava)
    }

  override def run (self: classmodel.Root): RN.Root =
    rFactory.createRoot before {
      _.getTables.addAll (self.getClasses.asScala.map (convertClass).asJava)
    }
