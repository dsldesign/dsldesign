// (c) dsldesign, wasowski, tberger
package dsldesign.relationalmodelfk.scala.transforms

import dsldesign.classmodel.{ClassmodelPackage, Root}
import dsldesign.scala.emf
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

@main def classModelToRelationalModel =

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", new XMIResourceFactoryImpl)

  // register the package magic (impure)
  ClassmodelPackage.eINSTANCE.eClass ()

  // load the class model
  val model:Root = emf.loadFromXMI ("dsldesign.classmodel/test-files/person-professor.xmi")

  // we run the transformation
  val classModel = ClassModelToRelationalModel.run (model)

  // save the relational model
  val outputPath = "dsldesign.relationalmodel/test-files/person-professor-transformed-by-Scala.xmi"
  emf.saveAsXMI (outputPath) (classModel)
