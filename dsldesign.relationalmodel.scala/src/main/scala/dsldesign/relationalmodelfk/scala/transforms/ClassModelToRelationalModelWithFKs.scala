// (c) dsldesign, wasowski, tberger
// An exogeneous transformation implemented directly in Scala (no special
// frameworks).  Translates a simple class model to a relational model with
// considering foreign keys (a classes's superclass's table is referenced by a foreign key).
package dsldesign.relationalmodelfk.scala.transforms

import scala.collection.mutable
import scala.jdk.CollectionConverters.*

import dsldesign.scala.emf.*
import dsldesign.{classmodel, relationalmodelfk => RN}

object ClassModelToRelationalModelWithFKs extends CopyingTrafo[classmodel.Root, RN.Root]:

  val rFactory = RN.RelationalmodelfkFactory.eINSTANCE

  def convertAttribute (self: classmodel.IntegerAttribute): RN.IntegerColumn =
    rFactory.createIntegerColumn before {
      _.setName (self.getName)
    }

  def convertClass 
    (transformedClasses: mutable.Map[classmodel.Class, RN.Table]) 
    (self: classmodel.Class): RN.Table = 
    transformedClasses.get (self) match
      case Some (table) => table
      case None =>
        rFactory.createTable before { t =>
          t.setName (self.getName)

          val key = rFactory.createIntegerColumn before { _.setName ("id") }
          t.getPrimaryKeys.add (key)
          t.getColumns.add (key)

          t.getColumns.addAll (self.getAttribute.asScala.map (convertAttribute).asJava)

          if (self.getSuperClass != null) {
            val fkey = rFactory.createIntegerColumn
            fkey.setName("fk_id")
            fkey.setRefersTo (convertClass(transformedClasses)(self.getSuperClass))
            t.getColumns.add(fkey)
          }
          transformedClasses.addOne (self, t)
        }

  override def run (self: classmodel.Root): RN.Root =
    val transformedClasses = mutable.Map[classmodel.Class,RN.Table] ()
    rFactory.createRoot before {
      _.getTables.addAll (self.getClasses.asScala.map (convertClass (transformedClasses)).asJava)
    }
