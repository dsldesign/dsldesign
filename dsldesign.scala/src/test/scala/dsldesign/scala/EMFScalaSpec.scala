// (c) dsldesign, wasowski, tberger
// Tests for the scala-emf adaptation
package dsldesign.scala.emf

import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.FsmFactory
import dsldesign.fsm.FsmPackage
import dsldesign.fsm.NamedElement
import dsldesign.fsm.State
import dsldesign.fsm.Model
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers


class emfSpec extends AnyFreeSpec with Matchers {

  // Helper functions

  def validates (name: String, ocl: Boolean) :Boolean = {
    val model = loadFromXMI[Model] (s"../dsldesign.fsm/test-files/${name}.xmi")
    val msgs = validate (model, ocl)
    for m <- msgs do { info (s"\t$m") }
    msgs.isEmpty
  }

  // initialize the package of the test model (a standard hack)
   FsmPackage.eINSTANCE.eClass

  val fnas = List ("test-00", "test-01", "test-02")
  val con  = fnas.map { name =>
    loadAllFromXMI[Model] (s"../dsldesign.fsm/test-files/${name}.xmi") }

  val factory = FsmFactory.eINSTANCE
  val emptyFSM : FiniteStateMachine = factory.createFiniteStateMachine

  // Tests

  "instanceOf" - {

    // fixture

    trait Alpha
    class A
    class AAlpha extends A with Alpha
    class B extends A
    class BAlpha extends AAlpha
    class C extends B
    class CAlpha extends BAlpha
    object Bobject extends BAlpha {}
    val iA = new A
    val iAAlpha = new AAlpha
    val iBAlpha = new BAlpha
    val iB = new B

    "filter direct instances and instances of subclasses" in {

      // first some sanity checks using a similar machinery of scalatest

      iA shouldBe an [A]
      iB shouldBe  a [B]
      iB shouldBe an [A]
      iB should not be a [C]
      iA should not be a [B]

      iAAlpha shouldBe an [Alpha]
      iAAlpha shouldBe an [A]
      iBAlpha shouldBe an [Alpha]

      iAAlpha should not be an [BAlpha]
      Bobject shouldBe a [BAlpha]
      Bobject should not be a [CAlpha]

      // now testing this with our simple API

      assert ( instanceOf[A](iA))
      assert ( instanceOf[B](iB))
      assert ( instanceOf[A](iB))
      assert (!instanceOf[C](iB))
      assert (!instanceOf[B](iA))

      assert ( instanceOf[Alpha](iAAlpha))
      assert ( instanceOf[A](iAAlpha))
      assert ( instanceOf[Alpha](iBAlpha))

      assert (!instanceOf[BAlpha](iAAlpha))
      assert ( instanceOf[BAlpha](Bobject))
      assert (!instanceOf[CAlpha](Bobject))
    }

  }


  "Inv" - {

    "type-check and compile" in {
      inv[FiniteStateMachine] { _ => true } // !_.getName.isEmpty }
    }

    "properly catch empty and null names" in {

      val c = dsldesign.scala.emf.inv[FiniteStateMachine]
        (m => m.getName != null && !m.getName.isEmpty)

      // I assert for each of the files separately for better error reporting

      assert ( con(0).forall { eo => c.check (eo) } )
      assert ( con(1).forall { eo => c.check (eo) } )
      assert ( con(2).forall { eo => c.check (eo) } )

      // we also need a failing test-case and so we have it

      assert ( !c.check (emptyFSM) )
      emptyFSM.setName ("")
      assert ( !c.check (emptyFSM) )
      emptyFSM.setName ("Test")
      assert ( c.check (emptyFSM) )

    }

  }



  "Conjoined constraints" - {

    def valid[T] :T => Boolean = _ => true
    def incon[T] :T => Boolean = _ => false

    "type-check and compile (invariant)" in {

      inv[FiniteStateMachine] (valid) &&
      inv[FiniteStateMachine] (incon) check
      emptyFSM shouldBe false

    }

    "type-check and compile (covariant check)" in {

      inv[NamedElement] (valid) check emptyFSM shouldBe true
      inv[NamedElement] (incon) check emptyFSM shouldBe false

    }

    "type-check and compile (conjunction of constraints)" in {

      // tests along the inheritance hierarchy
      val i1: Inv[FiniteStateMachine] =
        inv[NamedElement] (valid) && inv[FiniteStateMachine] (valid)
      val i2: Inv[FiniteStateMachine] =
        inv[FiniteStateMachine] (valid) && inv[NamedElement] (valid)
      val i3: Inv[FiniteStateMachine] =
        inv[FiniteStateMachine] (incon) && inv[NamedElement] (valid)

      i1 check emptyFSM shouldBe true
      i2 check emptyFSM shouldBe true
      i3 check emptyFSM shouldBe false

      // A test across the inheritance hierarchy.  This is a bit strange.  It
      // remains to be checked how it would work with multiple inheritance in
      // Ecore. Created an issue about that
      inv[FiniteStateMachine] (valid) && inv[State] (valid)

    }

    "be contravariant: allow down-cast only to narrower contexts" in {

      // invariant:
      val i1 = inv[FiniteStateMachine] (valid) : Inv[FiniteStateMachine]
      i1 check emptyFSM shouldBe true

      // contravariant:
      val i2 = inv[NamedElement] (valid).inCtx[FiniteStateMachine] : Inv[FiniteStateMachine]
      i2 check emptyFSM shouldBe true

      // covariant (should not compile):
      // inv[FiniteStateMachine] (valid) : Inv[NamedElement]

    }

  }

  "implies" - {

    "follow the truth table of implication" in {

      (true  implies true ) shouldBe true
      (true  implies false) shouldBe false
      (false implies true ) shouldBe true
      (false implies false) shouldBe true

    }

    "be lazy in the second argument" in {

      (false implies ???) shouldBe true
      intercept[scala.NotImplementedError] (true implies ???)

    }
  }

  // These tests should not be run in parallel as emf.validate is not thread safe.
  // If they are all placed in one suite, Scalatest will avoid parallel execution

  "OCL Ecore validation" - {

      "test-00 Ecore" in { validates ("test-00", false) shouldBe true }
      "test-00 OCL" in { validates ("test-00", true) shouldBe true }

      "test-01 Ecore" in { validates ("test-01", false) shouldBe true }
      "test-01 OCL" in { validates ("test-01", true) shouldBe true }

      "test-02 Ecore" in { validates ("test-02", false) shouldBe true }
      "test-02 OCL" in { validates ("test-02", true) shouldBe true }

      "test-03 Ecore" in { validates ("test-03", false) shouldBe true }
      "test-03 OCL" in { validates ("test-03", true) shouldBe false } // C5 should fail

      "test-04 Ecore" in { validates ("test-04", false) shouldBe false }
      "test-04 OCL" in { validates ("test-04", true) shouldBe false } // C2 should fail

      "test-05 Ecore" in { validates ("test-05", false) shouldBe true }
      "test-05 OCL" in { validates ("test-05", true) shouldBe true }

      "test-06 Ecore" in { validates ("test-06", false) shouldBe true }
      "test-06 OCL" in { validates ("test-06", true) shouldBe false } // C5 should fail

      "test-07 Ecore" in { validates ("test-07", false) shouldBe true }
      "test-07 OCL" in { validates ("test-07", true) shouldBe true }

      "test-08 Ecore" in { validates ("test-08", false) shouldBe true }
      "test-08 OCL" in { validates ("test-08", true) shouldBe false } // C1, C2 should fail

      "test-09 Ecore" in { validates ("test-09", false) shouldBe true }
      "test-09 OCL" in { validates ("test-09", true) shouldBe false } // C1 should fail

      "test-10 Ecore" in { validates ("test-10", false) shouldBe true }
      "test-10 OCL" in { validates ("test-10", true) shouldBe false } // C3 should fail

      "test-11 Ecore" in { validates ("test-11", false) shouldBe false }
      "test-11 OCL" in { validates ("test-11", true) shouldBe false } // C3 should fail, C5 might fail

      "test-12 Ecore" in { validates ("test-12", false) shouldBe true }
      "test-12 OCL" in { validates ("test-12", true) shouldBe false } // C4, C5 should fail

      "test-13 Ecore" in { validates ("test-13", false) shouldBe true }
      "test-13 OCL" in { validates ("test-13", true) shouldBe false } // C5, C6 should fail

      "test-14 Ecore" in { validates ("test-14", false) shouldBe true }
      "test-14 OCL" in { validates ("test-14", true) shouldBe false } // C2, C5 should fail

      "CoffeeMachine Ecore" in { validates ("CoffeeMachine", false) shouldBe true }
      "CoffeeMachine OCL" in { validates ("CoffeeMachine", true) shouldBe true }
  }
}
