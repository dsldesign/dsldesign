// (c) dsldesign, wasowski, tberger
// Example constraints implemented for the Pascal Triangle model in Scala
// This is the main runner for the constraints example
package dsldesign.pascal.scala

import scala.jdk.CollectionConverters.*

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import dsldesign.scala.emf.*
import dsldesign.pascal.PascalPackage

@main def main =

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put ("xmi", XMIResourceFactoryImpl ())

  // register the package magic (impure)
  PascalPackage.eINSTANCE.eClass ()

  // load the XMI file: change file name here (first create instance files if you wanna run this)
  val uri: URI = URI.createURI ("../dsldesign.pascal/test-files/test-00.xmi")
  val resource: Resource = ResourceSetImpl ().getResource (uri, true)

  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content: Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false).asScala

  if content.forall { eo => constraints.invariants.forall (_ check eo) } 
    then println ("All constraints are satisfied!")
    else println ("Some constraint is violated!")
