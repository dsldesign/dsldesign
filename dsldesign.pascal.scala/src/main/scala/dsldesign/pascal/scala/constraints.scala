// (c) dsldesign, wasowski, tberger
// An empty project to make writing Pascal triangle constraints in Scala easier.
package dsldesign.pascal.scala.constraints

import scala.jdk.CollectionConverters.* // for natural access to EList

import dsldesign.scala.emf.*
import dsldesign.pascal.*

val invariants: List[Constraint] = List (

  inv[Triangle] { _ => true }

)
