// (c) 2022 dsldesign, wasowski, tberger

package dsldesign.prpro.java;

import dsldesign.prpro.*;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

class ExprGeneratorMain {
  
  public static void main(String[] args) {

    Resource.Factory.Registry.INSTANCE
      .getExtensionToFactoryMap()
      .put("xmi", new XMIResourceFactoryImpl());
    PrproPackage.eINSTANCE.eClass();
    ResourceSet resourceSet = new ResourceSetImpl();

    if (args.length == 0) {
      System.err.println ("Error: give a path to an .xmi file as a cmd line argument. See README.md");
      return;
    }
    
    Model model = (Model) resourceSet
      .getResource (URI.createURI (args[0]), true)
      .getContents ().get (0);

    ExprGenerator gen = new ExprGenerator();

    for (Declaration d: model.getDecls()) {
      if (d instanceof Let) {
        Let let = (Let) d;
        String out = gen.generate (let.getValue (), let);
        System.out.println ("The declaration of ''" + let.getName() + "'");
        System.out.println (out);
        System.out.println ("-------------------------------------------");
      }
    }
  
  }

}
