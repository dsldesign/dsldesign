// (c) 2022 dsldesign, wasowski, tberger
package dsldesign.prpro.java;

import dsldesign.prpro.*;
import dsldesign.prpro.util.PrproSwitch;

class ExprGenerator {

  static public String generate (Expression e) 
  {
    return new GeneratorExprSwitch().doSwitch (e);
  }

  static public String generate (Expression e, Declaration context) 
  {
    GeneratorExprSwitch gen = new GeneratorExprSwitch (context);
    return stripParens (gen.doSwitch (e));
  }

  public static String stripParens (String s) {
    if (s.charAt (0) == '(' )
      return s.substring(1, s.length () - 1);
    else return s;
  }

  static class GeneratorExprSwitch extends PrproSwitch<String> 
  {
    Declaration context = null;

    public GeneratorExprSwitch () { }

    public GeneratorExprSwitch (Declaration context) {
      this.context = context;
    }

    private String lhs () {
      return (context != null) ? context.getName () : "";
    }

    private String observed () {
      if (context == null)
        return "";

      Model model = (Model) context.eResource().getContents().get(0);
      for (Declaration d: model.getDecls()) {
        if (d instanceof Data && d.getName().equals (context.getName()))
          return ", observed = data['" + d.getName() + "']";
      }

      return "";
    }

    @Override public String caseCstI (CstI expr)
    {
      return expr.getValue () + "";
    }

    @Override public String caseCstF (CstF expr)
    {
      return expr.getValue () + "";
    }

    @Override public String caseVarRef (VarRef expr)
    {
      String name = expr.getReferencedVar ().getName ();

      if (expr.getReferencedVar () instanceof Data)
        return "data['" + name + "']";
      else 
        return "prpro_" + name;
    }
    
    @Override public String caseBExpr (BExpr e)
    {
      String left = generate (e.getLeft ());
      String right = generate (e.getRight ());

      switch (e.getOperator ()) {
        case MINUS: return "(" + left + " - " + right + ")";
        case MULT:  return "(" + left + " * " + right + ")";
        case DIV:   return "(" + left + " / " + right + ")";
        default:    return "(" + left + " + " + right + ")";
      }
    }

    @Override public String caseNormal (Normal expr)
    {
      String mu = ", mu = " + stripParens (generate (expr.getMu ()));
      String sigma = ", sigma = " + stripParens (generate (expr.getSigma ()));
      String name = lhs () != "" ? "'" + lhs () + "', " : "";
      return "(Normal(" + name + mu + sigma + observed () + "))";
    }

    @Override public String caseUniform (Uniform expr)
    {
      String lo = stripParens (generate (expr.getLo ()));
      String hi = stripParens (generate (expr.getHi ()));
      String name = lhs () != "" ? "'" + lhs () + "', " : "";
      return "(Uniform(" + name + lo + ", " + hi + observed () + "))";
    }

  }
}
