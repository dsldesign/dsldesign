// (c) dsldesign, wasowski, tberger

// Helper functions for the types sub-language
package dsldesign.prpro.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.function.Supplier;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import dsldesign.prproTypes.*;
import dsldesign.prproTypes.util.PrproTypesSwitch;

class Types {

  private Types () { }

  public static final PrproTypesFactory tyFactory; 
  public static final SimpleTy floatTy;
  public static final SimpleTy probTy;
  public static final SimpleTy posProbTy;
  public static final SimpleTy posFloatTy;
  public static final SimpleTy natTy;
  public static final SimpleTy intTy;
  public static final SimpleTy nonNegFloatTy;
  public static final SimpleTy nonNegIntTy;

  static {

      dsldesign.prpro.PrproPackage.eINSTANCE.eClass ();
      tyFactory = PrproTypesFactory.eINSTANCE;

      floatTy = tyFactory.createSimpleTy ();
      floatTy.setTag (SimpleTyTag.FLOAT);

      probTy = tyFactory.createSimpleTy ();
      probTy.setTag (SimpleTyTag.PROB);

      posProbTy = tyFactory.createSimpleTy ();
      posProbTy.setTag (SimpleTyTag.POS_PROB);

      posFloatTy = tyFactory.createSimpleTy ();
      posFloatTy.setTag (SimpleTyTag.POS_FLOAT);

      nonNegFloatTy = tyFactory.createSimpleTy ();
      nonNegFloatTy.setTag (SimpleTyTag.NON_NEG_FLOAT);

      natTy = tyFactory.createSimpleTy ();
      natTy.setTag (SimpleTyTag.NAT);

      intTy = tyFactory.createSimpleTy ();
      intTy.setTag (SimpleTyTag.INT);

      nonNegIntTy = tyFactory.createSimpleTy ();
      nonNegIntTy.setTag (SimpleTyTag.NON_NEG_INT);
  }


  public static VectorTy vectorTy (int len, SimpleTy elemTy) 
  { 
    assert (elemTy != null);
    VectorTy result = tyFactory.createVectorTy ();
    result.setLen (len);
    result.setElemTy (EcoreUtil.copy (elemTy));
    return result; 
  }

  public static DistribTy distribTy (SimpleTy outcomeTy) 
  { 
    assert (outcomeTy != null);
    DistribTy result = tyFactory.createDistribTy ();
    result.setOutcomeTy (EcoreUtil.copy (outcomeTy));
    return result; 
  }

  public static Boolean isSubTypeOf (Ty t1, Ty t2) 
  {
    class SubTypeSwitch extends PrproTypesSwitch<Boolean> {
      public Boolean defaultCase (EObject t) { return false; }
    }

    assert (t1 != null);
    assert (t2 != null);
    return new SubTypeSwitch () {


      public Boolean caseSimpleTy (SimpleTy t1) {
        return new SubTypeSwitch () {

          // (SSimple)
          public Boolean caseSimpleTy (SimpleTy t2) 
          { return superTyTags (t1).contains (t2.getTag ()); }

        }.doSwitch (t2);
      }


      public Boolean caseVectorTy (VectorTy t1) {
        assert (t1.getElemTy () != null);
        return new SubTypeSwitch () {

          // (SVect-1)
          public Boolean caseVectorTy (VectorTy t2) { 
            return t2.getLen () <= t1.getLen () 
              && isSubTypeOf (t1.getElemTy (), t2.getElemTy ()); 
          }
          
          // (SVect-2)
          public Boolean caseSimpleTy (SimpleTy t2)
          { return isSubTypeOf (t1.getElemTy (), t2); }

        }.doSwitch (t2);
      }


      public Boolean caseDistribTy (DistribTy t1) {
        return new SubTypeSwitch () {
          
          // (SDist-1)
          public Boolean caseDistribTy (DistribTy t2) 
          { return isSubTypeOf (t1.getOutcomeTy (), t2.getOutcomeTy ()); }

          // (SDist-2)
          public Boolean caseSimpleTy (SimpleTy t2)
          { return isSubTypeOf (t1.getOutcomeTy (), t2); }

        }.doSwitch (t2);
      }

    }.doSwitch (t1);
  }

  public static Boolean isSuperTypeOf (Ty t1, Ty t2)
  { return isSubTypeOf (t2, t1); }

  protected static final List<SimpleTy> topologicallySortedSimpleTys = 
    List.of (natTy, posProbTy, nonNegIntTy, posFloatTy, probTy, intTy, 
      nonNegFloatTy, floatTy);

  public static SimpleTy lub (SimpleTy t1, SimpleTy t2)
  {
    assert (t1 != null);
    assert (t2 != null);
    SimpleTy result = topologicallySortedSimpleTys
      .stream ()
      .filter (t -> isSuperTypeOf (t,t1) && isSuperTypeOf (t,t2))
      .findFirst ()
      .orElse (null); // never used (an offensive null)

    assert (result != null);
    return result;
  }


  public static Ty lub (Ty t1, Ty t2)
  {
    class LubSwitch extends PrproTypesSwitch<Ty> {
      private EObject ty;
      public LubSwitch (EObject t) { this.ty = t; }
      public Ty get () { return this.doSwitch (ty); }
    }

    assert (t1 != null);
    return new LubSwitch (t1) {

      @Override
      public Ty caseSimpleTy (SimpleTy t1) 
      { return new LubSwitch (t2) {

          @Override
          public Ty caseSimpleTy (SimpleTy t2) 
          { return lub (t1, t2); }

          @Override
          public Ty caseDistribTy (DistribTy t2)
          { return lub (t1, t2.getOutcomeTy ()); }

          @Override
          public Ty caseVectorTy (VectorTy t2)
          { return lub (t1, t2.getElemTy ()); }

        }.get ();
      }

      @Override
      public Ty caseVectorTy (VectorTy t1) 
      {
        return new LubSwitch (t2) {

          @Override
          public Ty caseVectorTy (VectorTy t2) 
          {
            SimpleTy ty = lub (t1.getElemTy (), t2.getElemTy ());
            int len = Math.min (t1.getLen (), t2.getLen ());
            return vectorTy (len, ty);
          }

          // We would like to use caseTy, but we cannot because it will hide the
          // caseVectorTy. The Ecore implementation of switch matches abstract
          // types before the concrete types! So we need to do some code
          // duplication and separate distrbutions and simple types instead :(
          // But then we can get closer to the formal formulation of the rules
          @Override
          public Ty caseSimpleTy (SimpleTy t2)
          { return lub (t1.getElemTy (), t2); }

          @Override
          public Ty caseDistribTy (DistribTy t2)
          { return lub (t1.getElemTy (), t2.getOutcomeTy ()); }

        }.get ();
      }

      @Override
      public Ty caseDistribTy (DistribTy t1) 
      {
        return new LubSwitch (t2) 
        {
          @Override
          public Ty caseDistribTy (DistribTy t2) 
          { 
            SimpleTy ty = lub (t1.getOutcomeTy (), t2.getOutcomeTy ());
            return distribTy (ty); 
          }

          // We would like to use caseTy, but we cannot because it will hide the
          // caseDistribTy. The Ecore implementation of switch matches abstract
          // types before the concrete types! So we need to do some code
          // duplication and separate distrbutions and simple types instead :(
          // But then we can get closer to the formal formulation of the rules
          @Override
          public Ty caseSimpleTy (SimpleTy t2) 
          { return lub (t1.getOutcomeTy (), t2); }

          @Override
          public Ty caseVectorTy (VectorTy t2) 
          { return lub (t1.getOutcomeTy (), t2.getElemTy ()); }

        }.get ();
      }

    }.get ();
  }

  public static Set<SimpleTyTag> superTyTags (SimpleTy ty) 
  {
    List<SimpleTyTag> todo = new ArrayList<SimpleTyTag> ();
    Set<SimpleTyTag> ts = new HashSet<SimpleTyTag> ();
    todo.add (ty.getTag ());

    while (!todo.isEmpty ()) {
      SimpleTyTag t = todo.get (0);
      todo.remove (t);
      ts.add (t);
      todo.addAll (superTyTag (t));
    } 
    return ts;
  }

  // Immediate super types of type t
  public static Set<SimpleTyTag> superTyTag (SimpleTyTag t) 
  { 
    switch (t) {
      case INT:
        return Set.of (SimpleTyTag.FLOAT);
      case NON_NEG_INT:
        return Set.of (SimpleTyTag.INT, SimpleTyTag.NON_NEG_FLOAT);
      case NAT:
        return Set.of (SimpleTyTag.NON_NEG_INT, SimpleTyTag.POS_FLOAT);
      case FLOAT:
        return Set.of (); 
      case NON_NEG_FLOAT:
        return Set.of (SimpleTyTag.FLOAT);
      case POS_FLOAT:
        return Set.of (SimpleTyTag.NON_NEG_FLOAT);
      case PROB:
        return Set.of (SimpleTyTag.NON_NEG_FLOAT);
      case POS_PROB:
        return Set.of (SimpleTyTag.POS_FLOAT, SimpleTyTag.PROB);
      default:
        return Set.of ();
    }
  }

}

