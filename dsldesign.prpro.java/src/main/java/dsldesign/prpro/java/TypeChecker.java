// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.java;

import java.util.Map;
import java.util.HashMap;

import dsldesign.prpro.*;
import dsldesign.prproTypes.*;

import dsldesign.prpro.util.PrproSwitch;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

class TypeChecker {

  static public Map<String, Ty> emptyTypeEnv = 
    new HashMap<String, Ty> ();
  static public Map<String, Ty> emptyDataEnv = 
    new HashMap<String, Ty> ();

  static class TypeEnvs {

    public Map<String, Ty> let;
    public Map<String, Ty> data;

    public TypeEnvs() { 
      this.let = new HashMap (emptyTypeEnv);
      this.data = new HashMap (emptyDataEnv); 
    }
  }

  static public TypeEnvs emptyEnvs = new TypeEnvs ();

  static public TypeEnvs tyCheck (TypeEnvs env, Model model) 
    throws TypeError
  {
    for (Declaration decl: model.getDecls ())
      tyCheck (env, decl);
    return env;
  }

  static public TypeEnvs tyCheck (TypeEnvs env, Declaration decl) 
    throws TypeError
  { return new TyCheckDeclSwitch (env, decl).get (); }

  static public Ty tyCheck (TypeEnvs env, Expression expr) 
    throws TypeError
  { return new TyCheckExprSwitch (env, expr).get (); }


  static class TyCheckSwitch<T> extends PrproSwitch<T> {
    protected EObject ast;
    protected TypeEnvs env;

    public TyCheckSwitch (TypeEnvs env, EObject ast)
    {
      this.env = env;
      this.ast = ast;
    }

    public T get () { return this.doSwitch (ast); }

    @Override
    public T defaultCase (EObject ignore) throws TypeError
    { 
      throw new TypeError ("Internal Error (Should not happen). " +
          "Applied to object " + ignore); 
    }
  }


  static class TyCheckDeclSwitch extends TyCheckSwitch<TypeEnvs> 
  {
    public TyCheckDeclSwitch (TypeEnvs env, EObject ast) 
    { super (env, ast); }

    @Override public TypeEnvs caseData (Data decl) throws TypeError
    {
      String name = decl.getName ();
      if (this.env.data.containsKey (name))
        throw new TypeError 
          ("Identifier '" + name + "' has already been defined!");
      env.data.put (name, decl.getTy ());
      return env;
    }

    @Override public TypeEnvs caseLet (Let let) throws TypeError
    {
      String name = let.getName ();
      Ty t1 = tyCheck (this.env, let.getValue ());

      if (this.env.let.containsKey (name)) 
        throw new TypeError 
          ("Identifier '" + name + "' has already been defined!");
      
      this.env.let.put (name, t1);
      return env;
    }
  }


  static class TyCheckExprSwitch extends TyCheckSwitch<Ty> 
  {
    public TyCheckExprSwitch (TypeEnvs env, EObject ast) 
    { super (env, ast); }


    @Override public Ty caseCstI (CstI expr)
    {
      Ty result = Types.intTy;
      if (expr.getValue () > 0) 
        result = Types.natTy;
      else if (expr.getValue () == 0) 
        result = Types.nonNegIntTy;
      expr.setTy (result);
      return result;
    }

    @Override public Ty caseCstF (CstF expr)
    {
      Ty result = Types.floatTy;
      Double value = expr.getValue ();
      if (value == 0.0) 
        result = Types.probTy;
      else if (value > 0.0 && value <= 1.0) 
        result = Types.posProbTy;
      else if (value > 1.0)
        result = Types.posFloatTy;
      expr.setTy (result);
      return result;
    }

    @Override public Ty caseVarRef (VarRef expr) throws TypeError
    {
      Ty result;
      String name = expr.getReferencedVar ().getName ();
      // The reference existence is ensured by Ecore at this point.
      // But not that it is declared before in the file, which we require.
      if (this.env.data.containsKey (name))
        result = this.env.data.get (name);
      else if (!this.env.let.containsKey (name))
        throw new TypeError ("Variable '" + name + "' not declared before use!");
      else
        result = this.env.let.get (name);
      expr.setTy (result);
      return result;
    }
    
    // This rule is not sound for all our simple type -- operator
    // combinations.  We have an exercise to refine it.
    @Override public Ty caseBExpr (BExpr expr) throws TypeError
    {
      Ty t1 = tyCheck (this.env, expr.getLeft ());
      Ty t2 = tyCheck (this.env, expr.getRight ());
      Ty t = Types.lub (t1, t2);
      expr.setTy (t);
      return t;
    }


    @Override public Ty caseNormal (Normal expr) throws TypeError
    {
      Ty t1 = tyCheck (this.env, expr.getMu ());
      if (!Types.isSubTypeOf (t1, Types.floatTy))
        throw new TypeError (
         "Need a sub-type of FloatTy  for 'mu' but got '" + t1 +"'" );

      Ty t2 = tyCheck (this.env, expr.getSigma ());
      if (!Types.isSubTypeOf (t2, Types.nonNegFloatTy))
        throw new TypeError (
         "Need a sub-type of NonNegFloatTy for argument 'sigma' but seen '"
         + t2 +"'" );

      Ty t = Types.distribTy (Types.floatTy);
      expr.setTy (t);
      return t;
    }

    @Override public Ty caseUniform (Uniform expr)
    {
      Ty t0 = tyCheck (this.env, expr.getLo ());
      Ty t1 = tyCheck (this.env, expr.getHi ());
      Ty ty = Types.lub (t0, t1);
      SimpleTy t; 
      if (ty instanceof SimpleTy)
        t = (SimpleTy) ty;
      else if (ty instanceof DistribTy)
        t = ((DistribTy) ty).getOutcomeTy ();
      else throw new TypeError (
           "The supertype of extremes must be numeric but found '" + ty + "'!");

      expr.setTy (Types.distribTy (t));
      return expr.getTy ();
    }

  }


}
