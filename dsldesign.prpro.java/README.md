To execute the example expression generator implemented in Java invoke
the following in the main directory of the project
```
$ ./gradlew :dsldesign.prpro.java:run --args=../dsldesign.prpro/test-f
iles/example1.xmi
```
The output will be produced on the standard output, one expression per
Let declaration. (This generator does not translate the entire file,
but illustrates how to process expressions in a generator.)
