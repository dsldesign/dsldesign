# (c) 2022 wasowski, berger

# A reference example implementation for the code to be generated from a .prpro
# model. Running the script assumes that there are some date to use in Bayesian
# observations, so at least one variable that is not a dependency of anything
# else, and for which observation data exists.  Wy infer the posterior
# distribution, and generate a PDF file with some plots.

import arviz as az
import matplotlib.pyplot as plt
import pandas as pd
import pymc3 as pm

# A reference example in some rudimentary concrete syntax (this is what we are
# trying to implement):

# data x of vector of 500 positive_float
# data y of vector of 500 positive_float
# let β0 ~ Uniform (-2, 2)
# let β1 ~ Uniform (-20, 20)
# let σ ~ Uniform (0.001, 2.00)
# let y ~ Normal (β1 * x + β0, σ)
 
# We have abstract syntax for this model in
# dsldesign.prpro/test-files/example2.xmi

# We assume that the first row defines column names, and that there is a column
# 'x' and a column 'y', and that there are 500 of these numbers (but this is
# actually irrelevant for the PyMC3 code generator, as the size of the data set
# will be handled dynamically. We could have inserted an assertion)
data = pd.read_csv('./data.csv')

print('loading data.csv')
print(data.head())
print('...')

with pm.Model() as model:
    prpro_β0 = pm.Uniform('β0', -2, 2)
    prpro_β1 = pm.Uniform('β1', -20, 20)
    prpro_σ = pm.Uniform('σ', 0.001, 2.0)
    prpro_y = pm.Normal('y',
            mu = ((prpro_β1 * data['x']) + prpro_β0),
            sigma = prpro_σ,
            observed = data['y'])
    inferred_data = pm.sample(return_inferencedata = True)

# For simplicity of the example, let's assume a limitation that there are only
# two data columns,  the last one is the target and we plot all pair data plots
# for regressors to y (which one is the target could be inferred from the shape
# of the DAG, even on the Python side, by inspecting the graphical model, or on
# the AST side).

print('creating plots')

N = 3 # numbers of parameters (non-data backed variables) in the model

fig, ax = plt.subplots(2, N, figsize=(15, 10))
ax[0,0].plot(data['x'], data['y'], 'o', label='data', alpha = 0.3)
ax[0,0].set_xlabel('x')
ax[0,0].set_ylabel('y')
ax[0,0].set_title('A scatter plot of the data')

az.plot_posterior(inferred_data, var_names = ['β0'], ax = ax[1,0])
az.plot_posterior(inferred_data, var_names = ['β1'], ax = ax[1,1])
az.plot_posterior(inferred_data, var_names = ['σ'], ax = ax[1,2])
fig.tight_layout()

print(f'generating a pdf file {__file__}.pdf')
plt.savefig(f'{__file__}.pdf')
