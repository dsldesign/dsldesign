To run this particular example we recommending opening a virtual
environment as follows

```
$ python -m venv env
# (on some older systems the command is python3)

$ source env/bin/activate
$ pip install -r requirements.txt
```

However these, instructions seem flaky as pymc3 seem (especially
theano seems difficult to install with pip on diverse platforms,
because it has non-python dependencies).

If you use conda, you can try to create an environment for this
example:

```
conda create -n dsldesign.prpro --file requirements.txt
conda activate dsldesign.prpro
```

(this is more reliable than pip in our experience as of early 2022)

Once you have an installation, and the environment is active, you can
run

```
python reference-implementation.py
```

(or `python3` on some systems).  After about a minute it should
produce a file `reference-implementation.py.pdf`, which contais graphs
obtained from the model and a fake data set.  The dataset is
unintersting (a uniform sample from [0;1]x[0;1]) so the regression
plotted is also weak. But this is not important for us.  We want to
understand how to automate creating such a script from a model.
