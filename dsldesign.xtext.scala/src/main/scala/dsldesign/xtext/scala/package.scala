// (c) dsldesign, wasowski, tberger
//
// There are some difficulties in getting ParseHelp to work with scalatest and
// scala, so this file provides similar functionality using implicits and more
// natural scala style, so that we can unit tests grammars more easily.
package dsldesign.xtext.scala

import java.io.ByteArrayInputStream

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.XtextResourceSet

extension (in: String) (using setup: ISetup)
  def parse[T <: EObject]: Option[T] =
    // initialization
    val injector = setup.createInjectorAndDoEMFRegistration ()
    val resourceSet = injector.getInstance (classOf[XtextResourceSet])
    val extension = injector.getInstance (classOf[FileExtensionProvider])
                            .getPrimaryFileExtension ()
    val resource = resourceSet.createResource (URI.createURI ("." + extension))

    // starting the parser
    val input = ByteArrayInputStream (in.getBytes)
    resource.load (input, resourceSet.getLoadOptions)
    val contents = resource.getContents ()
    if contents.isEmpty () || !resource.getErrors.isEmpty ()
      then None
      else Some (contents.get (0).asInstanceOf[T])
